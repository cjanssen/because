Sound = {}

function initSound()
	loadSFX()
	loadMusic()

	Sound.mushroomPhase = 0
	Sound.mushroomRate = 6
	Sound.mushroomAmp = 0.25
end

function loadSFX()
	Sound.normalBullet = loadBank("snd/shot_normal")
	Sound.sinusBullet = loadBank("snd/shot_sinus")
	Sound.cheeseBullet = loadBank("snd/shot_cheese")
	Sound.cardBullet = loadBank("snd/shot_card")
	Sound.catBullet = loadBank("snd/shot_cat")
	Sound.sushiBullet = loadBank("snd/shot_sushi")
	Sound.bottleBullet = loadBank("snd/shot_bottle")
	Sound.heartBullet = loadBank("snd/shot_heart")
	Sound.flowerBullet = loadBank("snd/shot_flower")
	Sound.smallBullet = loadBank("snd/shot_small")

	Sound.bulletExplosion = loadBank("snd/bulletExplosion")
	Sound.enemyExplosion = loadBank("snd/enemyExplosion")
	Sound.satelliteExplosion = loadBank("snd/satelliteExplosion")
	Sound.pickup = loadBank("snd/pickup")
	Sound.extraLife = loadBank("snd/extralife")
	Sound.playerDeath = loadBank("snd/playerDeath")
end

function loadMusic()
	Sound.introMusic = love.audio.newSource("mus/beginLoop.ogg","stream")
	Sound.introMusic:setLooping(true)
	Sound.introMusic:setVolume(0.7)
	Sound.gameplayMusic = {
		current = 1,
		love.audio.newSource("mus/ingameLoop.ogg","stream"),
		love.audio.newSource("mus/ingameLoop2.ogg","stream"),
		love.audio.newSource("mus/ingameLoop3.ogg","stream")
	}
	for i,music in ipairs(Sound.gameplayMusic) do
		music:setLooping(false)
		music:setVolume(0.7)
	end
	Sound.ingameMusic = Sound.gameplayMusic[1]
	Sound.pauseMusic = love.audio.newSource("mus/pawsLoop.ogg","stream")
	Sound.pauseMusic:setLooping(true)
	Sound.pauseMusic:setVolume(0.7)
	Sound.pauseMusic:play()
	Sound.pauseMusic:pause()
	Sound.endMusic = love.audio.newSource("mus/endLoop.ogg","stream")
	Sound.endMusic:setLooping(false)
	Sound.endMusic:setVolume(0.7)

	Sound.inGame = false
end

function resetMusicPlaylist()
	Sound.gameplayMusic.current = 1
	Sound.ingameMusic = Sound.gameplayMusic[Sound.gameplayMusic.current]
end

function stopAllMusic()
	Sound.introMusic:pause()
	Sound.ingameMusic:pause()
	Sound.pauseMusic:pause()
	Sound.endMusic:pause()
	Sound.inGame = false
end

function startStopMusic()
	if pauseScreen.soundIsOn then
		if pauseScreen.paused then
			playPauseMusic(true)
		else
			if gameState == 1 then
				playIntroMusic(true)
			elseif gameState == 3 then
				playEndMusic(true)
			else
				playIngameMusic(true)
			end
		end
	else
		stopAllMusic()
	end 
end

function playIntroMusic(skiprewind)
	stopAllMusic()
	if not skiprewind then
		Sound.introMusic:rewind()
		Sound.introMusic:play()
	end
	Sound.introMusic:resume()
	if not pauseScreen.soundIsOn then
		Sound.introMusic:pause()
	end
end

function playIngameMusic(skiprewind)
	stopAllMusic()
	if not skiprewind then
		Sound.ingameMusic:rewind()
		Sound.ingameMusic:play()
	end
	Sound.ingameMusic:resume()
	Sound.ingameMusic:setVolume(0.7)
	Sound.ingameMusic:setPitch(1)
	if not pauseScreen.soundIsOn then
		Sound.ingameMusic:pause()
	end
	Sound.inGame = true
end

function playPauseMusic(skiprewind)
	stopAllMusic()
	if not skiprewind then
		Sound.pauseMusic:rewind()
		Sound.pauseMusic:play()
	end
	Sound.pauseMusic:resume()
	if not pauseScreen.soundIsOn then
		Sound.pauseMusic:pause()
	end
end

function playEndMusic(skiprewind)
	stopAllMusic()
	if not skiprewind then
		Sound.endMusic:rewind()
		Sound.endMusic:play()
	end
	Sound.endMusic:resume()
	if not pauseScreen.soundIsOn then
		Sound.endMusic:pause()
	end
end

function playSound(snd, pos, var, vol)
	if not pauseScreen.soundIsOn then return end
	snd = snd or Sound.playerShoot
	pan = pos/screenSize.x or 0.5
	pan = math.max(0.0, math.min(pan,1.0))

	snd.last = (snd.last % table.getn(snd)) + 1
	local ls = snd[1][snd.last]
	local rs = snd[2][snd.last]
	ls:stop()
	ls:rewind()
	if var then ls:setPitch(1+var*(math.random()-0.5)) end
	local lvol = vol or 1
	ls:setVolume(lvol * math.sin(pan*math.pi/2))
	ls:setPosition(1,0,0)
	
	rs:stop()
	rs:rewind()
	if var then rs:setPitch(1+var*(math.random()-0.5)) end
	local rvol = vol or 1
	rs:setVolume(rvol * math.cos(pan*math.pi/2))
	rs:setPosition(-1,0,0)

	rs:play()
	ls:play()
end

function playSoundMono(snd, pos, var, vol)
	if not pauseScreen.soundIsOn then return end
	snd = snd or Sound.playerShoot
	pan = pos/screenSize.x or 0.5
	pan = math.max(0.0, math.min(pan,1.0))

	snd.last = (snd.last % table.getn(snd)) + 1
	local rs = snd[2][snd.last]
	rs:stop()
	rs:rewind()
	if var then rs:setPitch(1+var*(math.random()-0.5)) end
	local rvol = vol or 1
	rs:setVolume(rvol)
	rs:setPosition(pan*2-1,0,0)
	rs:play()
end


function loadBank(fname)
	local retval = {{},{}}
	local snddta = love.sound.newSoundData(fname.."_l.ogg")
	for i=1,16 do
		table.insert(retval[1],love.audio.newSource(snddta))
	end
	snddta = love.sound.newSoundData(fname.."_r.ogg")
	for i=1,16 do
		table.insert(retval[2],love.audio.newSource(snddta))
	end

	retval.last = 1
	return retval
end


function enableMushroomSound()
	Sound.mushroomEnabled = true
end

function disableMushroomSound()
	Sound.mushroomEnabled = false
	Sound.ingameMusic:setPitch(1)
end

function updateMushroomSound(dt)
	if Sound.mushroomEnabled then
		Sound.mushroomPhase = (Sound.mushroomPhase + Sound.mushroomRate * dt) % (2*math.pi)
		Sound.ingameMusic:setPitch(0.9 + Sound.mushroomAmp * (math.sin(Sound.mushroomPhase)))
	end
end

function updateMusic(dt)
	if Sound.inGame and Sound.ingameMusic:isStopped() then
		local n = table.getn(Sound.gameplayMusic)
	--	local m = math.random(n-1)
	--	for i=1,m do
		Sound.gameplayMusic.current = Sound.gameplayMusic.current % n + 1

	--	end
		Sound.ingameMusic = Sound.gameplayMusic[Sound.gameplayMusic.current]
		playIngameMusic(false)
	end
end