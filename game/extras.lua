
function hsv2rgb(h, s, v, a)
    if s <= 0 then return v,v,v,a end
    h, s, v = h/360*6, s/255, v/255
    local c = v*s
    local x = (1-math.abs((h%2)-1))*c
    local m,r,g,b = (v-c), 0,0,0
    if h < 1     then r,g,b = c,x,0
    elseif h < 2 then r,g,b = x,c,0
    elseif h < 3 then r,g,b = 0,c,x
    elseif h < 4 then r,g,b = 0,x,c
    elseif h < 5 then r,g,b = x,0,c
    else              r,g,b = c,0,x
    end 
	return (r+m)*255,(g+m)*255,(b+m)*255,a
end

function normalize(vec)
	local mod = math.sqrt(vec.x*vec.x + vec.y*vec.y)
	if mod == 0 then return vec end
	vec = { x = vec.x/mod, y = vec.y/mod }
	return vec
end

function signOf(a)
	if a<0 then return -1 end
	return 1
end

function randomSign()
	if math.random()<0.5 then return -1 else return 1 end
end


function outOfScreen(vec, w, h)
	if vec.x + w < 0 then return true end
	if vec.y + h < 0 then return true end
	if vec.x - w > bgImg:getWidth()*1.5 then return true end
	if vec.y - h > screenSize.y then return true end
	return false
end

function outOfScreenSide(vec, w, h)
	if vec.x + w < 0 then return true end
	if vec.x - w > bgImg:getWidth()*1.5 then return true end
	if vec.y - h > screenSize.y then return true end
	return false
end

function outOfScreenTops(vec, w, h)
	if vec.y + h < 0 then return true end
	if vec.y - h > screenSize.y then return true end
	return false
end

function belowScreen(vec, h)
	if vec.y - h > screenSize.y then return true end
	return false
end

function belowOneHalfScreen(vec, h)
	if vec.y - h > screenSize.y*1.5 then return true end
	return false
end

function checkBoxes(entityA, entityB)
	if entityA.pos.x - entityA.w/2 > entityB.pos.x + entityB.w/2 then return false end
	if entityA.pos.x + entityA.w/2 < entityB.pos.x - entityB.w/2 then return false end
	if entityA.pos.y - entityA.h/2 > entityB.pos.y + entityB.h/2 then return false end
	if entityA.pos.y + entityA.h/2 < entityB.pos.y - entityB.h/2 then return false end
	return true
end

function increaseExponential(dt, var, amount)
	if var < 1 then
		var = 1 - (1 - var) * math.pow(amount, 60*dt)
		if var > 0.999 then
			var = 1
		end
	end
	return var
end

function decreaseExponential(dt, var, amount)
	if var > 0 then
		var = var * math.pow(amount, 60*dt)
		if var < 0.001 then
			var = 0
		end
	end
	return var
end

function indexOf(list, value)
	for i,v in ipairs(list) do
		if v == value then return i end
	end
	return -1
end

function randn()
	local x1,x2
	local w = 1
	while w >= 1 and w>0 do
		x1 = 2 * math.random() - 1
		x2 = 2 * math.random() - 1
		w = x1*x1 + x2*x2
	end

	w = math.sqrt( (-2 * math.log( w ) / w ) )
	return x1 * w
end

function addToBatch(batch, frame, px, py, ang, sx, sy, ox, oy)
	px = px or 0
	py = py or 0
	ang = ang or 0
	sx = sx or 1
	sy = sy or 1
	ox = ox or 0
	oy = oy or 0

	if loveVersion == 9 then
		batch:add(frame, px, py,ang,sx,sy,ox,oy)
	else
		batch:addq(frame, px, py,ang,sx,sy,ox,oy)
	end
end

function printShadowedText(txt, x, y, opacity, centered)
	opacity = opacity or 1
	if centered then
		y = y - font:getHeight()/2
		x = x - font:getWidth(txt)/2
	end
	-- poor man's shadow (imagefonts mess with kerning/separation/etc)
	love.graphics.setColor(166,4,56,64*opacity)
	love.graphics.print(txt,x-1,y-1)
	love.graphics.print(txt,x+1,y-1)
	love.graphics.print(txt,x-1,y+1)
	love.graphics.print(txt,x+1,y+1)

	love.graphics.setColor(255,255,255,255*opacity)
	love.graphics.print(txt,x,y)
end
