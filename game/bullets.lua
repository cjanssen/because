Bullets = {}

function initBullets()
	Bullets.loadPics()
end

function startBullets()
	Bullets.playerList = {}
	Bullets.enemyList = {}
end

function Bullets.wipe()
	startBullets()
end

function Bullets.addPlayerBullet(startPos)
	local ang1 = 15*math.pi/180 - Player.p.vel.y * 1.05
	if ang1 < 0 then ang1 = 0 end
	local ang2 =  - Player.p.vel.y * 0.9
	if ang2 < 0 then ang2 = 0 end

	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x - 11*math.cos(Player.p.angle), y = startPos.y - 11*math.sin(Player.p.angle)},
		speed = 1500,
		dir = { x = math.cos(-math.pi/2 - ang1 + Player.p.angle), y = math.sin(-math.pi/2 - ang1 + Player.p.angle)},
		w = 6,
		h = 24,
		fromPlayer = true,
		frameIdx = 2,
		angle = Player.p.angle - ang1 
	})
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x - 11*math.cos(Player.p.angle), y = startPos.y - 11*math.sin(Player.p.angle)},
		speed = 1500,
		dir = { x =  math.cos(-math.pi/2 - ang2 + Player.p.angle), y = math.sin(-math.pi/2 - ang2 + Player.p.angle)},
		w = 6,
		h = 24,
		fromPlayer = true,
		frameIdx = 2,
		angle = Player.p.angle - ang2
	})
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x + 11*math.cos(Player.p.angle), y = startPos.y + 11*math.sin(Player.p.angle) },
		speed = 1500,
		dir = { x =  math.cos(-math.pi/2 + ang2 + Player.p.angle), y = math.sin(-math.pi/2 + ang2 + Player.p.angle)},
		w = 6,
		h = 24,
		fromPlayer = true,
		frameIdx = 2,
		angle = Player.p.angle + ang2
	})
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x + 11*math.cos(Player.p.angle), y = startPos.y + 11*math.sin(Player.p.angle)},
		speed = 1500,
		dir = { x = math.cos(-math.pi/2 + ang1 + Player.p.angle), y = math.sin(-math.pi/2 + ang1 + Player.p.angle)},
		w = 6,
		h = 24,
		fromPlayer = true,
		frameIdx = 2,
		angle = Player.p.angle + ang1
	})
end

function Bullets.addSimplePlayerBullet(startPos)
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x - 11*math.cos(Player.p.angle), y = startPos.y - 11*math.sin(Player.p.angle)},
		speed = 1500,
		dir = { x =  math.cos(-math.pi/2 + Player.p.angle), y = math.sin(-math.pi/2 + Player.p.angle)},
		w = 6,
		h = 24,
		fromPlayer = true,
		frameIdx = 2,
		angle = Player.p.angle
	})
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x + 11*math.cos(Player.p.angle), y = startPos.y + 11*math.sin(Player.p.angle) },
		speed = 1500,
		dir = { x =  math.cos(-math.pi/2 + Player.p.angle), y = math.sin(-math.pi/2 + Player.p.angle)},
		w = 6,
		h = 24,
		fromPlayer = true,
		frameIdx = 2,
		angle = Player.p.angle
	})
end

function Bullets.addSinusBullet(startPos)
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x , y = startPos.y },
		speed = 800,
		dir = { x = 0, y = -1},
		w = 6,
		h = 6,
		fromPlayer = true,
		frameIdx = 2,
		angle = 0,

		bulletType = BulletTypes.sinus, -- 1: sinus
		center = { x = startPos.x+fgOvr.x , y = startPos.y },
		phase = 0,
		amp = 140,
		freq = 20,
		canExitOnSides = true
	})
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x , y = startPos.y },
		speed = 800,
		dir = { x = 0, y = -1},
		w = 6,
		h = 6,
		fromPlayer = true,
		frameIdx = 2,
		angle = 0,

		bulletType = BulletTypes.sinus, -- 1: sinus
		center = { x = startPos.x+fgOvr.x , y = startPos.y },
		phase = 0,
		amp = 140,
		freq = -20,
		canExitOnSides = true
	})
end

function Bullets.addCheeseBullet(startPos)
	local n = 3
	local s = (n*2)+1
	for i = -n,n do
		table.insert(Bullets.playerList, {
			pos = { x = startPos.x+fgOvr.x , y = startPos.y },
			speed = 900 + math.random(300),
			dir = { x = math.sin(i/s*math.pi), y = -math.cos(i/s*math.pi)},
			w = 32,
			h = 32,
			fromPlayer = true,
			frameIdx = 1,
			angle = 0,

			bulletType = BulletTypes.cheese, -- 2: cheese
			explodes = true,
			specialFrame = true,
			slowdown = 0.9,
			disapThres = 10
		})
	end
end

function Bullets.addCardBullet(startPos)
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x , y = startPos.y },
		speed = 800,
		dir = { x = 0, y = -1},
		w = 16,
		h = 16,
		fromPlayer = true,
		frameIdx = 6,
		angle = 0,

		bulletType = BulletTypes.card, -- 1: grav
		explodes = true,
		specialFrame = true,
		gravVel = { x = 14, y = 1 },
		gravDir = { x = -math.cos(0.2), y = math.sin(0.2)},
		gravAmount = 0.08,
		canExitOnSides = true

	})
end

function Bullets.addCatBullet(startPos)
	local ang = math.random() * math.pi * 2
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x , y = startPos.y },
		speed = 1350,
		dir = { x = 0, y = -1 },
		w = 64,
		h = 64,
		fromPlayer = true,
		frameIdx = 3,
		angle = 0,

		bulletType = BulletTypes.cat, -- 4: cats
		explodes = true,
		specialFrame = true,
		slowdown = 0.9,
		newDir = { x = math.cos(ang), y = math.sin(ang) },
		newSpeed = 200,
		drift = math.random() * math.pi * 4
	})
end

function Bullets.addSushiBullet(startPos)
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x , y = startPos.y },
		speed = 1000,
		dir = { x = 0, y = -1 },
		w = 128,
		h = 128,
		fromPlayer = true,
		frameIdx = 4,
		angle = 0,

		bulletType = BulletTypes.sushi, -- 5: sushi
		explodes = true,
		specialFrame = true,
		slowdown = 0.9,
		disapThres = 10,
		scale = 1.5
	})
end

function Bullets.addSubSushiBullet(startPos)
	local phase = math.random()*math.pi*2
	for i=0,6 do
		table.insert(Bullets.playerList, {
			pos = { x = startPos.x , y = startPos.y },
			speed = 400,
			dir = { x = math.cos(i/7*math.pi*2), y = -math.sin(i/7*math.pi*2)},
			w = 64,
			h = 64,
			fromPlayer = true,
			frameIdx = 4,
			angle = phase,

			bulletType = BulletTypes.subsushi, -- 6: subsushi
			specialFrame = true,
			explodes = true,
			scale = 0.8,
			drift = math.random() * math.pi * 4
		})
	end
end

function Bullets.addBottleBullet(startPos)
	math.log(1-math.random())
	local sign = 1
	if math.random() < 0.5 then sign = -1 end

	local ang = (math.log(1-math.random())*sign*1/12+0.5) * math.pi
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x , y = startPos.y },
		speed = 500,
		dir = { x = math.cos(ang), y = -math.sin(ang)},
		w = 16,
		h = 16,
		fromPlayer = true,
		frameIdx = 2,
		angle = 0,

		bulletType = BulletTypes.bottle, -- 7: bottle
		explodes = true,
		specialFrame = true,
		gravVel = { x = 0, y = -1 },
		gravDir = { x = 0, y = 1},
		gravAmount = 0.035,
		canExitOnSides = true

	})
end

function Bullets.addHeartBullet(startPos)
	local ne = table.getn(Enemies.list)
	local etarget = false
	if ne > 0 then
		local ndx = math.random(ne)
		etarget = Enemies.list[ndx]
	end
	table.insert(Bullets.playerList, {
		pos = { x = startPos.x+fgOvr.x , y = startPos.y },
		speed = 300,
		dir = { x = 0, y = -1},
		w = 16,
		h = 16,
		fromPlayer = true,
		frameIdx = 5,
		angle = 0,

		bulletType = BulletTypes.heart, -- 8: heart
		explodes = true,
		specialFrame = true,
		drift = math.random() * math.pi * 2,
		target = etarget
	})
end

function Bullets.addFlowerBullet(startPos)
	local n = 7
	for i = 1,n do
		table.insert(Bullets.playerList, {
			pos = { x = startPos.x+fgOvr.x , y = startPos.y },
			speed = 400 + math.random(150),
			dir = { x = math.sin(i/n*math.pi*2), y = -math.cos(i/n*math.pi*2)},
			w = 32,
			h = 32,
			fromPlayer = true,
			frameIdx = 8,
			angle = 0,
			scale = 1,
			step = 1,

			bulletType = BulletTypes.flower, -- 9: flower
			explodes = true,
			specialFrame = true,
			slowdown = 0.95,
			disapThres = 100,
			drift = (math.random()-0.5) * math.pi * 2 * 4
		})
	end
end

function Bullets.addSubFlowerBullet(bullet)
	local ang = math.atan2(bullet.dir.y,bullet.dir.x)
	local sc = 0.6 * bullet.scale
	local n = 1
	if bullet.step == 1 then
		n = 4
		ang = ang - math.pi * 3/8
	elseif bullet.step == 2 then
		n = 2
		ang = ang - math.pi * 1/8
	end

	for i = 0,n-1 do
		table.insert(Bullets.playerList, {
			pos = { x = bullet.pos.x , y = bullet.pos.y },
			speed = (400 + math.random(150)) * sc,
			dir = { x = math.cos(ang + i*math.pi/4), y = math.sin(ang + i*math.pi/4)},
			w = 32 * sc,
			h = 32 * sc,
			fromPlayer = true,
			frameIdx = 8,
			angle = 0,
			scale = sc,
			step = bullet.step + 1,

			bulletType = BulletTypes.flower, -- 9: flower
			explodes = true,
			specialFrame = true,
			slowdown = 0.95,
			disapThres = bullet.disapThres * sc,
			drift = (math.random()-0.5) * math.pi * 2 * 4
		})
	end
end


function Bullets.addEnemyBullet(startPos)
	table.insert(Bullets.enemyList, {
		pos = { x = startPos.x, y = startPos.y },
		speed = 300,
		dir = { x = 0, y = 1},
		w = 6,
		h = 6,
		angle = 0,
		fromPlayer = false,
		animated = true,
		frameIdx = 1,
		frameTimer = 0,
		frameDelay = 0.04
	})
end

function Bullets.addEnemyDirBullet(startPos, startAngle)
	table.insert(Bullets.enemyList, {
		pos = { x = startPos.x, y = startPos.y },
		speed = 250,
		dir = { x = math.cos(startAngle + math.pi/2), y = math.sin(startAngle + math.pi/2)},
		w = 6,
		h = 6,
		angle = 0,
		fromPlayer = false,
		animated = true,
		frameIdx = 1,
		frameTimer = 0,
		frameDelay = 0.04
	})
end

function Bullets.addEnemyFasterDirBullet(startPos, startAngle)
	table.insert(Bullets.enemyList, {
		pos = { x = startPos.x, y = startPos.y },
		speed = params.fasterBulletSpeed(),
		dir = { x = math.cos(startAngle + math.pi/2), y = math.sin(startAngle + math.pi/2)},
		w = 6,
		h = 6,
		angle = 0,
		fromPlayer = false,
		animated = true,
		frameIdx = 1,
		frameTimer = 0,
		frameDelay = 0.04
	})
end

function Bullets.addHomingBullet(startPos)
	-- get player direction
	local vec = normalize({
		x = Player.p.pos.x+fgOvr.x - startPos.x,
		y = Player.p.pos.y - startPos.y })

	table.insert(Bullets.enemyList, {
		pos = { x = startPos.x, y = startPos.y },
		speed = params.homingBulletSpeed(),
		dir = vec,
		w = 6,
		h = 6,
		fromPlayer = false,
		animated = true,
		frameIdx = 1,
		frameTimer = 0,
		frameDelay = 0.04
	})
end

function Bullets.addLaserBullet(startPos, shootAngle)
	-- get player direction
	local direction = { x = math.cos(1/2*math.pi + shootAngle), y = math.sin(1/2*math.pi + shootAngle)}
	table.insert(Bullets.enemyList, {
		pos = { x = startPos.x, y = startPos.y },
		speed = params.laserBulletSpeed(),
		angle = shootAngle,
		dir = direction,
		extraGrav = 150,
		w = 6,
		h = 6,
		fromPlayer = false,
		isLaser = true
	})
end

function Bullets.addEnemyFourLasers(startPos, scanted)
	local ang = 0
	if scanted then ang = math.pi/4 end
	-- get player direction
	for i=1,4 do
		local ng = ang + i*math.pi/2
		table.insert(Bullets.enemyList, {
			pos = { x = startPos.x, y = startPos.y },
			dir = { x = math.cos(ng), y = math.sin(ng)},
			speed = params.laserBulletSpeed(),
			angle = ng + math.pi/2,
			extraGrav = 150,
			w = 6,
			h = 6,
			fromPlayer = false,
			isLaser = true
		})
	end
end

function Bullets.addMgLaserBullet(startPos)
	local bias = { 
		x = (math.random()-0.5) * params.mgBulletXBias(), 
		y = (math.random()-0.5) * params.mgBulletYBias() }

	table.insert(Bullets.enemyList, {
			pos = { x = startPos.x + bias.x, y = startPos.y + bias.y },
			dir = { x = 0, y = 1},
			speed = params.mgBulletSpeed(),
			angle = 0,
			w = 6,
			h = 6,
			fromPlayer = false,
			isLaser = true
		})
end

function Bullets.addEnemyPlantBullets(startPos)
	local n = params.plantBulletCount()
	local spacing = params.plantBulletSpacing()
	local diff = Player.p.pos.x + fgOvr.x - startPos.x
	local mode = 0
	if diff < -200 then
		mode = 1
	elseif diff > 200 then
		mode = 2
	end
	local vg = 2
	for i=1,n do
		local x0 = -i*spacing
		if mode == 0 then
			x0 = (i - n/2) * spacing
			if x0 == 0 then x0 = -n/2*spacing end
		elseif mode == 2 then
			x0 = i*spacing
		end
		-- local y0 = -0.5 - (i%2)*0.25
		local y0 = -0.6
		local spd = 300
		local alpha = math.pow(60/(spd*math.abs(x0)), vg/60/(1-y0))
		table.insert(Bullets.enemyList, {
			pos = { x = startPos.x, y = startPos.y },
			speed = spd,
			dir = { x = x0, y = y0},
			w = 6,
			h = 6,
			angle = 0,
			
			plantBullet = true,
			vGrav = vg,
			horzDrag = alpha,

			fromPlayer = false,
			frameIdx = 1,
			frameTimer = 0,
			frameDelay = 0.04
		})
	end
end

function Bullets.addRasberryBullet(startPos, startDir)
	local ang = math.random() * 2 * math.pi
	local spd = params.raspberryBulletSpeed()
	table.insert(Bullets.enemyList, {
		pos = { x = startPos.x, y = startPos.y },
		speed = spd,
		dir = { x = math.cos(ang) + startDir.x * 100/spd, y = math.sin(ang) + startDir.y * 100/spd},
		w = 28,
		h = 28,
		angle = 0,
		drift = (math.random()-0.5) * math.pi * 2,
		life = params.raspberryBulletLife(),
		raspberryBullet = true,

		specialFrame = true,
		frameIdx = 12,
		explodes = true,

		fromPlayer = false
	})
end

function Bullets.loadPics()
	-- right leaning frames
	local img = love.graphics.newImage("img/shots4.png")
	Bullets.enemyFrames = {}
	Bullets.batch = love.graphics.newSpriteBatch(img, 2000, "static")
	for i=0,1 do
		table.insert(Bullets.enemyFrames, 
			love.graphics.newQuad(i*64, 6*64, 64, 64, img:getWidth(), img:getHeight()))
	end

	Bullets.playerFrames = {}
	table.insert(Bullets.playerFrames,
		love.graphics.newQuad(128, 64*7, 64, 64, img:getWidth(), img:getHeight()))

	for i=0,1 do
		table.insert(Bullets.playerFrames,
			love.graphics.newQuad(i*64, 64*7, 64, 64, img:getWidth(), img:getHeight()))
	end

	Bullets.extraFrames = {}
	for j=0,1 do
		for i=0,5 do
			table.insert(Bullets.extraFrames,
				love.graphics.newQuad(i*64, 64*(5-j), 64, 64, img:getWidth(), img:getHeight()))
		end
	end
end

function Bullets.scheduleKill(list, num, silent)
	if silent then
		list[num].explodes = false
	end
	list[num].dead = true
end

function Bullets.purge()
	function purgeList(list)
		local n = table.getn(list)
		for i=1,n do
			if list[n+1-i].dead then
				Bullets.kill(list, n+1-i)
			end
		end
	end
	purgeList(Bullets.playerList)
	purgeList(Bullets.enemyList)
end

function Bullets.kill(list, num)
	local bullet = list[num]
	if bullet.explodes then
		Explo.add(bullet.pos, (bullet.bulletType == BulletTypes.flower and bullet.step == 3))
		if bullet.bulletType == BulletTypes.flower then
			playSound(Sound.bulletExplosion,bullet.pos.x-fgOvr.x,0.7,math.pow(bullet.scale,0.8))
		else
			playSound(Sound.bulletExplosion,bullet.pos.x-fgOvr.x,0.7)
		end
	end
	table.remove(list, num)
end

function Bullets.update(dt)
	Bullets.purge()
	Bullets.updatePlayerBullets(dt)
	Bullets.updateEnemyBullets(dt)
end

function Bullets.updatePlayerBullets(dt)
	for i,v in ipairs(Bullets.playerList) do
		-- store pos
		local prePos = { x = v.pos.x, y = v.pos.y }
		-- position
		v.pos.x = v.pos.x + v.dir.x * v.speed * dt
		v.pos.y = v.pos.y + v.dir.y * v.speed * dt
		
		if v.bulletType == BulletTypes.sinus then
			-- sinus bullet
			v.center.x = v.center.x + v.dir.x * v.speed * dt
			v.center.y = v.center.y + v.dir.y * v.speed * dt
			v.phase = v.phase + v.freq * dt
			v.pos.x = v.center.x + v.amp * math.sin(v.phase)
			v.pos.y = v.center.y

			-- rotate
			local quad = math.floor(v.phase%math.pi)/(math.pi/2)
			local refphase = v.phase - 0.1
			if quad > math.pi/4 then refphase = v.phase + 0.1 end
			-- local refphase = v.phase
			local refangle = (refphase % (math.pi*2))
			v.angle = signOf(v.freq) * (-(refphase % math.pi)  + math.pi/2 )
			if refangle > math.pi then
				v.angle = signOf(v.freq) * ((refphase % math.pi) - math.pi/2)
			end
		elseif v.bulletType == BulletTypes.cheese then
			-- cheese
			v.speed = v.speed * math.pow(v.slowdown,dt*60)
			if v.speed < v.disapThres then
				Bullets.scheduleKill(Bullets.playerList, i)
			end
		elseif v.bulletType == BulletTypes.card then
			-- card
			v.gravVel.x = v.gravVel.x + v.gravDir.x * v.speed * v.gravAmount * dt
			v.gravVel.y = v.gravVel.y + v.gravDir.y * v.speed * v.gravAmount * dt
			v.pos.x = v.pos.x + v.gravVel.x
			v.pos.y = v.pos.y + v.gravVel.y

			-- angle
			local diff = { x = v.pos.x - prePos.x, y = v.pos.y - prePos.y }
			v.angle = math.atan2(diff.y, diff.x) + math.pi/2
		elseif v.bulletType == BulletTypes.cat then
			-- cat
			local factor = math.pow(v.slowdown, dt*60)
			v.speed = v.speed * factor + v.newSpeed * (1-factor)
			v.dir.x = v.dir.x * factor + v.newDir.x * (1-factor)
			v.dir.y = v.dir.y * factor + v.newDir.y * (1-factor)
			v.angle = v.angle + v.drift * dt
		elseif v.bulletType == BulletTypes.sushi then
			-- sushi (big)
			v.speed = v.speed * math.pow(v.slowdown,dt*60)
			if v.speed < v.disapThres then
				Bullets.addSubSushiBullet(v.pos)
				Bullets.scheduleKill(Bullets.playerList, i)
			end
		elseif v.bulletType == BulletTypes.subsushi then
			-- sushi (small)
			v.angle = v.angle + v.drift * dt
		elseif v.bulletType == 7 then
			-- bottle
			v.gravVel.x = v.gravVel.x + v.gravDir.x * v.speed * v.gravAmount * dt
			v.gravVel.y = v.gravVel.y + v.gravDir.y * v.speed * v.gravAmount * dt
			v.pos.x = v.pos.x + v.gravVel.x
			v.pos.y = v.pos.y + v.gravVel.y

			-- angle
			local diff = { x = v.pos.x - prePos.x, y = v.pos.y - prePos.y }
			v.angle = math.atan2(diff.y, diff.x) + math.pi/2
		elseif v.bulletType == BulletTypes.heart then
			-- heart

			-- retarget if necessary
			if not v.target or not v.target.alive then 
				local ne = table.getn(Enemies.list)
				if ne > 0 then
					local ndx = math.random(ne)
					v.target = Enemies.list[ndx]
				else
					v.target = false
				end
			end

			-- follow target
			if v.target and v.target.alive then
				local diffvec = { 
					x = v.target.pos.x - v.pos.x,
					y = v.target.pos.y - v.pos.y }
				v.dir = normalize(diffvec)
			end
			v.angle = v.angle + v.drift * dt
		elseif v.bulletType == BulletTypes.flower then
			-- flower
			v.speed = v.speed * math.pow(v.slowdown,dt*60)
			if v.speed < v.disapThres then
				Bullets.scheduleKill(Bullets.playerList, i)
				if v.step < 3 then
					-- flower: fractalize
					Bullets.addSubFlowerBullet(v)
				end
			end
			v.angle = v.angle + v.drift * dt
		end

		if v.canExitOnSides then
			if outOfScreenTops(v.pos, v.w/2, v.h/2) then
				Bullets.scheduleKill(Bullets.playerList, i, true)
			end
		else
			if outOfScreen(v.pos, v.w/2, v.h/2) then
				Bullets.scheduleKill(Bullets.playerList, i, true)
			end
		end
	end
end

function Bullets.updateEnemyBullets(dt)
	for i,v in ipairs(Bullets.enemyList) do
		-- position
		v.pos.x = v.pos.x + v.dir.x * v.speed * dt
		v.pos.y = v.pos.y + v.dir.y * v.speed * dt

		if v.extraGrav then
			v.pos.y = v.pos.y + v.extraGrav * dt
		end

		if v.plantBullet then
			if v.dir.y < 1 then
				v.dir.x = v.dir.x * math.pow(v.horzDrag, dt*60)
				v.dir.y = v.dir.y + v.vGrav * dt
			else
				v.dir.y = 1
				v.dir.x = 0
			end
		end

		if v.raspberryBullet then
			v.angle = v.angle + v.drift * dt
			v.life = v.life - dt
			if v.life <= 0 then
				Bullets.scheduleKill(Bullets.enemyList, i)
			end
		end

		if outOfScreen(v.pos, v.w/2, v.h/2) then
			Bullets.scheduleKill(Bullets.enemyList, i, true)
		end

		-- animation
		if v.animated and v.frameTimer then
			v.frameTimer = v.frameTimer - dt
			if v.frameTimer <= 0 then 
				v.frameTimer = v.frameDelay 
				v.frameIdx = (v.frameIdx % table.getn(Bullets.enemyFrames)) + 1
			end
		end
	end
end


function Bullets.draw()
	Bullets.batch:clear()
	for i,v in ipairs(Bullets.playerList) do
		if v.specialFrame then
			local sc = v.scale or 1
			-- Bullets.batch:add(Bullets.extraFrames[v.frameIdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, sc, sc, 32, 32)
			addToBatch(Bullets.batch,Bullets.extraFrames[v.frameIdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, sc, sc, 32, 32)

		else
			-- Bullets.batch:add(Bullets.playerFrames[v.frameIdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, 1, 1, 32, 32)
			addToBatch(Bullets.batch,Bullets.playerFrames[v.frameIdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, 1, 1, 32, 32)
		end
	end
	for i,v in ipairs(Bullets.enemyList) do
		if v.isLaser then
			-- Bullets.batch:add(Bullets.playerFrames[2], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, 1, 1, 32, 32)
			addToBatch(Bullets.batch,Bullets.playerFrames[2], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, 1, 1, 32, 32)

		elseif v.specialFrame then
			local sc = v.scale or 1
			-- Bullets.batch:add(Bullets.extraFrames[v.frameIdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, sc, sc, 32, 32)
			addToBatch(Bullets.batch,Bullets.extraFrames[v.frameIdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, sc, sc, 32, 32)
		else
			-- Bullets.batch:add(Bullets.enemyFrames[v.frameIdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), 0, 1, 1, 32, 32)
			addToBatch(Bullets.batch,Bullets.enemyFrames[v.frameIdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), 0, 1, 1, 32, 32)
		end
	end
	love.graphics.draw(Bullets.batch)
end