Formations = {}

function initFormations()
	Formations.enemyProgression = { 1, 3, 2, 5, 4, 12, 6, 11, 7, 10, 8, 9 }

	Formations.powerupGroups = {
		good = {2, 4, 14, 13},
		medium = {3, 15, 5, 6, 8, 7},
		bad = {9, 11, 10, 12}
	}
	Formations.powerupOrder = { 3, 15, 5, 6, 4, 11, 2, 12, 14, 10, 7, 8, 13, 9 }
end

function startFormations()

	Formations.delay = 21
	-- Formations.timer = 21
	Formations.timer = 0
	Formations.insertSet = {1,2,3,4}
	Formations.pickSet = Formations.parsePickIndexes(Formations.insertSet)
	Formations.pickPointer = 4

	Formations.powerupRefs = {1,2,3,4,5}
	Formations.powerupSet = Formations.getPowerupSet(Formations.powerupRefs)
	Formations.powerupPointer = 5
	Formations.newWave(true)
end

function updateFormations(dt)
	Formations.timer = Formations.timer - dt
	if Formations.timer <= 0 then
		Formations.newWave(false)
	end
end

function Formations.parsePickIndexes(pickSet)
	local tmp = {}
	-- translate
	for i,v in ipairs(pickSet) do
		table.insert(tmp,Formations.enemyProgression[v])
	end

	-- randomize
	local ret = {}
	while table.getn(tmp)>0 do
		table.insert(ret,table.remove(tmp,math.random(table.getn(tmp))))
	end

	return ret
end

function Formations.changeSet()
	-- remove 2
	for i=1,2 do
		table.remove(Formations.insertSet,math.random(table.getn(Formations.insertSet)))
	end

	-- choose next 2
	while table.getn(Formations.insertSet) < 4 do
		Formations.pickPointer = Formations.pickPointer + 1
		if Formations.pickPointer > table.getn(Formations.enemyProgression) then
			Formations.pickPointer = 1
		end
		if not Formations.contains(Formations.insertSet, Formations.pickPointer) then
			table.insert(Formations.insertSet, Formations.pickPointer)
		end
	end

	-- generate pick set
	Formations.pickSet = Formations.parsePickIndexes(Formations.insertSet)
end

function Formations.getPowerupSet(refs)
	local pool,ret
	local tries = 12

	repeat
		pool,ret = {},{}

		-- generate candidate pair
		for i,v in ipairs(refs) do
			table.insert(pool,Formations.powerupOrder[v])
		end

		table.insert(ret, table.remove(pool,math.random(table.getn(pool))))
		table.insert(ret, table.remove(pool,math.random(table.getn(pool))))

		-- after 12 failed tries, just accept whatever you have
		tries = tries - 1
		if tries <= 0 then return ret end
	
		-- check if valid
		if Formations.contains(Formations.powerupGroups.good,ret[1]) and not
			Formations.contains(Formations.powerupGroups.bad,ret[2]) then
			ret = {}
		elseif Formations.contains(Formations.powerupGroups.bad,ret[1]) and not
			Formations.contains(Formations.powerupGroups.good,ret[2]) then
			ret = {}
		elseif Formations.contains(Formations.powerupGroups.medium,ret[1]) and not
			Formations.contains(Formations.powerupGroups.medium,ret[2]) then
			ret = {}
		end
	until table.getn(ret) == 2

	return ret
end

function Formations.changePickupSet()
	-- remove 2
	local n = table.getn(Formations.powerupRefs)

	for i=1,2 do
		table.remove(Formations.powerupRefs,math.random(table.getn(Formations.powerupRefs)))
	end

	-- choose next 2
	while table.getn(Formations.powerupRefs) < n do
		Formations.powerupPointer = Formations.powerupPointer + 1
		if Formations.powerupPointer > table.getn(Formations.powerupOrder) then
			Formations.powerupPointer = 1
		end
		if not Formations.contains(Formations.powerupRefs, Formations.powerupPointer) then
			table.insert(Formations.powerupRefs, Formations.powerupPointer)
		end
	end

	-- get set
	Formations.powerupSet = Formations.getPowerupSet(Formations.powerupRefs)
end

function Formations.contains(tabl, n)
	for i,v in ipairs(tabl) do
		if v == n then return true end
	end
	return false
end

function Formations.getEnemyIndex()
	local nextProb = 0.6
	if math.random() < nextProb then
		return Formations.pickSet[1]
	elseif math.random() < nextProb then
		return Formations.pickSet[2]
	else
		return Formations.pickSet[3]
	end
end

function Formations.getPickups()
	local choice = math.random(3)
	if choice == 1 then 
		return 1
	elseif choice == 2 then
		return Formations.powerupSet[1]
	else
		return Formations.powerupSet[2]
	end
end 

function Formations.newWave(skipChange)
	if not skipChange then
		if Player.p.alive then
			Formations.changeSet()
			Formations.changePickupSet()
			progressionStep()
		end
	end
	Formations.delay = params.waveTime()
	Formations.timer = Formations.timer + Formations.delay

	-- give the player a break (6 seconds)
	local breakDuration = params.waveBreak()
	Enemies.spawner.timer = Enemies.spawner.timer + breakDuration
	Formations.timer = Formations.timer + breakDuration
end

--------------------------------------------
-- patterns

function Formations.clearpat(w,h)
	local pat = {}
	for j=1,h do
		pat[j] = {}
		for i=1,w do
			pat[j][i] = 0
		end
	end
	return pat
end

function Formations.getPattern(desiredCount)
		local bestScore = -1
		local pat = {}
		repeat
			local n = math.random(6)
			if desiredCount==1 then pat = {{math.random(2)}}
			elseif n == 1 then pat = Formations.generateCheckerboard(desiredCount)
			elseif n == 2 and desiredCount <= 44 then pat = Formations.generateCross(desiredCount)
			elseif n == 3 then pat = Formations.generateTriangle(desiredCount)
			elseif n == 4 and desiredCount <= 18 then pat = Formations.generateBars(desiredCount)
			elseif n == 5 then pat = Formations.generateCircle(desiredCount)
			elseif n == 6 then pat = Formations.generateFractal(desiredCount)
			end
		until table.getn(pat)>0

		local partialCounts = {0,0}
		for j,w in ipairs(pat) do
			for i,v in ipairs(w) do
				if v ~= 0 then partialCounts[v] = partialCounts[v] + 1 end
			end
		end

		local sel = 0
		partialCounts[3] = partialCounts[1]+partialCounts[2]
		for i,v in ipairs(partialCounts) do
			local score = math.abs(v - desiredCount)
			if score < bestScore or bestScore == -1 then
				bestScore = score
				sel = i
			end
		end

		if sel ~= 0 then
			for j,w in ipairs(pat) do
				for i,v in ipairs(w) do
					if v == 2 and sel==1 then pat[j][i]=0 end
					if v == 1 and sel==2 then pat[j][i]=0 end
				end
			end
		end
	return Formations.getTrimmed(pat)
end

function Formations.getTrimmed(patin)
	local w,h = table.getn(patin),table.getn(patin[1])
	local top,bottom,left,right = 1,h,1,w
	local leftok,rightok = true,true
	for i=1,w do
		for j=1,h do
			if leftok and patin[i][j] ~= 0 then
				left = i
				leftok = false
			end
			if rightok and patin[w-i+1][j] ~= 0 then
				right = w-i+1
				rightok = false
			end
		end
	end

	local topok,bottomok = true,true
	for j=1,h do
		for i=1,w do
			if topok and patin[i][j] ~= 0 then
				top = j
				topok = false
			end
			if bottomok and patin[i][h-j+1] ~= 0 then
				bottom = h-j+1
				bottomok = false
			end
		end
	end

	local ret = {}
	for i=left,right do
		ret[i-left+1] = {}
		for j=top,bottom do
			ret[i-left+1][j-top+1] = patin[i][j]
		end
	end

	return ret
end

function Formations.generateCheckerboard(desiredCount)
	local D = math.random(2) * desiredCount
	local rw = math.max(1,(math.random(5)-3)+math.floor(math.sqrt(D)))
	local cl = math.ceil(D/rw)
	rw = math.ceil(D/cl)
	local ua = math.random(2)
	pat = {}
	for j=1,cl do
		pat[j] = {}
		for i=1,rw do
			pat[j][i] = (i+j+ua)%2+1
		end
	end

	return pat
end

function Formations.generateCross(desiredCount)
	local topcount = math.max(math.floor(desiredCount/4)-(math.random(2)-1),0)
	local toppiece = {}
	local tr,tc = math.random(3),math.random(3)
	if tr*tc < topcount then 
		tc = math.min(math.ceil(topcount/tr),3)
		tr = math.min(math.ceil(topcount/tc),3)
	end
	local locations = {}
	for i=1,tc do for j=1,tr do table.insert(locations,{i,j}) end end
	for i=1,tc do
		toppiece[i] = {}
		for j=1,tr do toppiece[i][j] = 0 end
	end

	local putpieces = 0
	for i=1,topcount do
		local n = table.getn(locations)
		if n == 0 then break end
		local pos = table.remove(locations,math.random(n))
		toppiece[pos[1]][pos[2]] = math.random(2)
		putpieces = putpieces + 1
	end
	topcount = putpieces

	local centercount = math.min(math.max(desiredCount - topcount * 4, 0),8)

	local centerpiece = {}
	local cr,cc = math.random(2),math.random(3)+1
	if cr*cc < topcount then 
		cr = math.min(math.ceil(topcount/cc),4)
		cc = math.min(math.ceil(topcount/cr),2)
	end

	if centercount%2 == 1 and cc ~= 3 then cc = 3 end
	for i=1,cc do
		centerpiece[i] = {}
		for j=1,cr do centerpiece[i][j] = 0 end
	end
	locations = {}
	for i=1,math.ceil(cc) do for j=1,cr do table.insert(locations,{i,j}) end end
	repeat
		local pos = table.remove(locations,math.random(table.getn(locations)))
		if centerpiece[pos[1]][pos[2]] == 0 then
			local color = math.random(2)
			centerpiece[pos[1]][pos[2]] = color
			centercount = centercount - 1 
			if centerpiece[cc-pos[1]+1][pos[2]] == 0 then
				centerpiece[cc-pos[1]+1][pos[2]] = color
				centercount = centercount - 1
			end
		end
	until centercount <= 0 or table.getn(locations)==0


	-- mount all
	local patc = tc*2+cc
	local patr = tr*2+cr

	-- prepare pattern template
	local pat = Formations.clearpat(patc,patr)


	-- copy centerpiece
	for i=1,cc do
		for j=1,cr do
			pat[j+tr][i+tc] = centerpiece[i][j]
		end
	end

	-- copy sides
	for i=1,tc do
		for j=1,tr do
			pat[j][i] = toppiece[i][j]
			pat[j][patc+1-i] = toppiece[i][j]
			pat[patr+1-j][i] = toppiece[i][j]
			pat[patr+1-j][patc+1-i] = toppiece[i][j]
		end
	end
	return pat
end


function Formations.generateBars(desiredCount)
	local len = math.random(4)+2
	local dir = math.random(3)
	local cnt = math.random(2)

	local linecount = 1
	if desiredCount > 6 then
		dir = math.random(2)
		if desiredCount > 2 then
			cnt = 2
		end
	end
	if dir~=3 then linecount = linecount + cnt end
	len = math.min(6, math.ceil(desiredCount/linecount))

	local patw = len
	local path = len
	if dir == 1 then
		patw = len + 3 * cnt
	elseif dir == 2 then
		path = len + 3 * cnt
	end

	local pat = Formations.clearpat(patw,path)

	for i=1,len do
		local v = math.random(2)
		pat[i][i] = v
		if dir == 2 then
			pat[i+3][i] = v
			if cnt == 2 then
				pat[i+6][i] = v
			end
		elseif dir == 1 then
			pat[i][i+3] = v
			if cnt == 2 then
				pat[i][i+6] = v
			end
		end
	end

	if math.random(2)==1 then
		-- reverse
		local patc = {}
		for j=1,path do
			patc[j] = {}
			for i=1,patw do
				patc[j][patw + 1 - i] = pat[j][i]
			end
		end
		pat = patc
	end
	return pat
end

function Formations.generateTriangle(desiredCount)
	local rows = math.max(1, math.ceil(math.sqrt(desiredCount)) + math.random(4))
	local cols = rows*2-1

	local count = desiredCount
	local locations = {}
	for j = 1,rows do
		for i=j,rows do
			if ((i+j)%2) == 1 then table.insert(locations,{j,i}) end
		end
	end

	local pat = Formations.clearpat(cols,rows)
	repeat
		local pos = table.remove(locations,math.random(table.getn(locations)))
		if pat[pos[1]][pos[2]] == 0 then
			local color = math.random(2)
			pat[pos[1]][pos[2]] = color
			count = count - 1
			if pat[pos[1]][cols+1-pos[2]] == 0 then
				count = count -1
			end
			pat[pos[1]][cols+1-pos[2]] = color
		end
	until count <= 0 or table.getn(locations)== 0
	return pat
end

function Formations.generateCircle(desiredCount)
	local perims = {1,4,8,16,20,32,32,36,48,56,60,64,84,84}
	local rad = 0
	for i,v in ipairs(perims) do
		if v<=desiredCount/2 then rad = i end
	end
	rad = rad + math.random(2)-1
	local inrad = math.floor(rad/2)

	local sz = rad*2-1
	local pat = Formations.clearpat(sz,sz)
	local c = math.random(2)
	for j = 1,sz do
		for i = 1,sz do
			local r = math.ceil(math.sqrt((i-rad)*(i-rad)+(j-rad)*(j-rad)))
			if r > rad or r < inrad then
				pat[j][i] = 0
			else
				pat[j][i] = ((r+c)%2)+1
			end
		end
	end
	return pat
end

function Formations.generateFractal(desiredCount)
	local incnt = math.floor(math.sqrt(desiredCount)+0.5)
	local w = math.max(2,math.ceil(math.sqrt(incnt) + math.random(3)-1))
	local h = math.max(2,math.ceil(incnt/w + math.random(3)-1))

	local base = Formations.clearpat(w,h)
	local locations = {}
	for j=1,h do for i=1,w do table.insert(locations,{j,i}) end end

	local count = incnt
	repeat
		local pos = table.remove(locations,math.random(table.getn(locations)))
		base[pos[1]][pos[2]] = math.random(2)
		count = count - 1
	until count <= 0 or table.getn(locations) == 0
	
	local pat = Formations.clearpat(w*w,h*h)
	for l=1,h do 
		for k=1,w do
			for j=1,h do 
				for i=1,w do
					if base[l][k] ~= 0 then
						pat[j+(l-1)*h][i+(k-1)*w] = base[j][i]
					end
				end 
			end
		end
	end
	return pat
end
