function initLifeMeter()
	LifeMeters = {}
	LifeSpawner = {}
	loadLifeMeterPics()
end

function loadLifeMeterPics()
	local img = love.graphics.newImage("img/lifemeters2.png")
	LifeMeters.sprites = {}
	LifeMeters.batch = love.graphics.newSpriteBatch(img,10,"static")

	for i=0,1 do
		table.insert(LifeMeters.sprites,
			love.graphics.newQuad(i*64, 0, 64, 64, img:getWidth(), img:getHeight()))
	end

	img = love.graphics.newImage("img/miniship.png")
	LifeSpawner.frames = {}
	LifeSpawner.batch = love.graphics.newSpriteBatch(img,10,"static")
	for i=0,1 do
		table.insert(LifeSpawner.frames,
			love.graphics.newQuad(i*64, 0, 64, 64, img:getWidth(), img:getHeight()))
	end
	LifeSpawner.haloImg = love.graphics.newImage("img/halo.png")
end

function drawLifeMeter()
	LifeMeters.batch:clear()
	for i=0,Player.p.lives-1 do
		-- LifeMeters.batch:add(LifeMeters.sprites[1], math.floor((i-Player.p.maxLives/2)*20 + screenSize.x / 2), 14, 0, s, s, 32, 32)
		addToBatch(LifeMeters.batch,LifeMeters.sprites[1], math.floor((i-Player.p.maxLives/2)*20 + screenSize.x / 2), 14, 0, s, s, 32, 32)
	end

	for i = 0, Player.p.maxLives - Player.p.lives-1 do
		-- LifeMeters.batch:add(LifeMeters.sprites[2], math.floor((i+Player.p.lives-Player.p.maxLives/2)*20 + screenSize.x / 2), 14, 0, s, s, 32, 32)
		addToBatch(LifeMeters.batch,LifeMeters.sprites[2], math.floor((i+Player.p.lives-Player.p.maxLives/2)*20 + screenSize.x / 2), 14, 0, s, s, 32, 32)
	end
	love.graphics.draw(LifeMeters.batch)
end

function startLifeSpawner()
	LifeSpawner.list = {}
	startLifeSpawnerTimer()

	-- special: first "educational" life
	LifeSpawner.aboutToStart = true
	LifeSpawner.first = true
end

function startLifeSpawnerTimer()
	LifeSpawner.time = 15
	LifeSpawner.delay = 15
end

function spawnLife()
	local px = (math.random()-0.5) * 200 + Player.p.pos.x + fgOvr.x
	if px < 24 then px = 24 end
	if px > bgImg:getWidth()*1.5 - 24 then px = bgImg:getWidth()*1.5 - 24 end 
	table.insert(LifeSpawner.list, {
		pos = { x = px, y = -24 },
		dir = { x = 0, y = 1 },
		w = 48,
		h = 48,
		speed = 200,
		currentFrame = 1,
		frameTime = 0.2,
		frameDelay = 0.2
		})

	if LifeSpawner.aboutToStart then
		LifeSpawner.first = true
		LifeSpawner.aboutToStart = false
	end
end
function collectLife(life)
	life.dead = true
	playSound(Sound.extraLife, Player.p.pos.x)
	if Player.p.lives < Player.p.maxLives then
		Player.p.lives = Player.p.lives + 1
	end
end

function updateLifeSpawner(dt)
	-- spawn new lives
	LifeSpawner.time = LifeSpawner.time - dt
	if LifeSpawner.time <= 0 and Player.p.lives < Player.p.maxLives and Player.p.lives > 0 and table.getn(LifeSpawner.list) < 1 and not gameFinished then
		spawnLife()
		LifeSpawner.time = LifeSpawner.time + LifeSpawner.delay
	end

	-- update collectibles
	for i,v in ipairs(LifeSpawner.list) do
		-- movement
		v.pos.x = v.pos.x + v.speed * v.dir.x * dt
		v.pos.y = v.pos.y + v.speed * v.dir.y * dt
		if belowScreen(v.pos, v.h/2) then
			v.dead = true
		end

		-- animation
		v.frameTime = v.frameTime - dt
		while v.frameTime <= 0 do
			v.frameTime = v.frameTime + v.frameDelay
			v.currentFrame = 3 - v.currentFrame
		end
	end

	-- prune
	local n = table.getn(LifeSpawner.list)
	for i = 0,n-1 do
		if LifeSpawner.list[n-i].dead then
			table.remove(LifeSpawner.list, n-i)
		end
	end
end

function drawLifeSpawner()
	-- draw collectibles
	LifeSpawner.batch:clear()
	for i,v in ipairs(LifeSpawner.list) do
		love.graphics.draw(LifeSpawner.haloImg, math.floor(v.pos.x - fgOvr.x), math.floor(v.pos.y), 0, 1, 1, 32, 32)
		-- LifeSpawner.batch:add(LifeSpawner.frames[v.currentFrame], math.floor(v.pos.x - fgOvr.x), math.floor(v.pos.y), 0, 1, 1, 32, 32)	
		addToBatch(LifeSpawner.batch,LifeSpawner.frames[v.currentFrame], math.floor(v.pos.x - fgOvr.x), math.floor(v.pos.y), 0, 1, 1, 32, 32)	
	end
	love.graphics.draw(LifeSpawner.batch)

	for i,v in ipairs(LifeSpawner.list) do
		printShadowedText("EXTRA LIFE",
			math.floor(v.pos.x - fgOvr.x),
			math.floor(v.pos.y - 32), 1, true)
	end
end
