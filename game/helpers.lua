Satellites = {}
function initSatellites()
	Satellites.list = {}
end

function startSatellites()
	Satellites.list = {}
	Satellites.phase = 0
	Satellites.freq = 4
end

function Satellites.respawn()
	local startPos = Player.p.pos
	-- do not repeat
	local idlist = {}
	for i,v in ipairs(Satellites.list) do
		table.insert(idlist, v.id)
	end

	local n = 9
	for i=1,n do
		local ndx = indexOf(idlist,i)
		if ndx == -1 or Satellites.list[ndx].hooked == false then
		table.insert(Satellites.list, {
				pos = { x = startPos.x+fgOvr.x , y = startPos.y },
				speed = 500 + math.random()*100,
				dir = { x = math.sin(math.pi*2), y = -math.cos(math.pi*2)},
				w = 16,
				h = 16,
				frameIdx = 11,
				angle = 0,
				drift = (math.random()-0.5)*math.pi*3,
				scale = 1,

				hooked = true,

				phase = i/n*math.pi*2,
				ampfraction = 0,
				amprate = 0.94,
				radius = 80,
				id = i
			})
		else
			-- table.remove(idlist,ndx)
		end
	end
end

function Satellites.discard()
	for i,v in ipairs(Satellites.list) do
		v.hooked = false
	end
end

function Satellites.scheduleKill(i)
	Satellites.list[i].dead = true
end

function Satellites.kill(i)
	table.remove(Satellites.list, i)
end

function Satellites.update(dt)
	Satellites.phase = (Satellites.phase + Satellites.freq * dt) % (math.pi*2)
	for i,v in ipairs(Satellites.list) do
		-- position
		if v.hooked then
			v.ampfraction = increaseExponential(dt, v.ampfraction, v.amprate)
			local phase = v.phase + Satellites.phase
			v.pos.x = Player.p.pos.x + fgOvr.x + v.ampfraction * v.radius * math.cos(phase)
			v.pos.y = Player.p.pos.y + v.ampfraction * v.radius * math.sin(phase)
			v.dir = { x = math.cos(phase + math.pi/2), y = math.sin(phase + math.pi/2) }
		else
			v.pos.x = v.pos.x + v.dir.x * v.speed * dt
			v.pos.y = v.pos.y + v.dir.y * v.speed * dt
		end

		v.angle = v.angle + v.drift * dt

		if not v.hooked and outOfScreen(v.pos, v.w/2, v.h/2) then
			Satellites.scheduleKill(i)
			-- Explo.add(v.pos)
		else
			Satellites.checkBullets(i)
			Satellites.checkEnemyCollision(i)
		end
	end

	-- purge
	local n = table.getn(Satellites.list)
	for i=0,n-1 do
		if Satellites.list[n-i].dead then
			Satellites.kill(n-i)
		end		
	end
end

function Satellites.checkBullets(ndx)
	local sat = Satellites.list[ndx]
	for i,v in ipairs(Bullets.enemyList) do
		if not v.dead and checkBoxes(v, sat) then
			Satellites.scheduleKill(ndx)
			Explo.add(sat.pos)
			playSound(Sound.satelliteExplosion,v.pos.x - fgOvr.x,0.3)
			Bullets.scheduleKill(Bullets.enemyList,i)
		end
	end
end

function Satellites.checkEnemyCollision(ndx)
	local sat = Satellites.list[ndx]
	for i,v in ipairs(Enemies.list) do
		if v.alive and checkBoxes(v,sat) then
			Satellites.scheduleKill(ndx)
			Explo.add(sat.pos)
			playSound(Sound.satelliteExplosion,v.pos.x - fgOvr.x,0.3)
			Enemies.scheduleExplode(v)
		end
	end
end

function Satellites.draw()
	Bullets.batch:clear()
	for i,v in ipairs(Satellites.list) do
		local sc = v.scale or 1
		-- Bullets.batch:add(Bullets.extraFrames[v.frameIdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, sc, sc, 32, 32)
		addToBatch(Bullets.batch,Bullets.extraFrames[v.frameIdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, sc, sc, 32, 32)
	end
	love.graphics.draw(Bullets.batch)
end
