Pickups = {}

-- Pickups:
-- 1. normal 4-way shoot
-- 2. shoot in sinusoids -> laser
-- 3. shotgun-like -> cheese
-- 4. shoot in curve (lateral gravity) -> card
-- 5. explosive cats drifting -> cat
-- 6. fireworks with recoil -> sushi
-- 7. fountain (vertical gravity) -> bottle
-- 8. gun of love (homing hearts) -> heart
-- 9. solar wind (ship is pulled in a direction that changes)
-- 10. ship rotates over its axis
-- 11. magic mushroom (color+distortion)
-- 12. super-inertia
-- 13. temporal invulnerability / transparency
-- 14. flower (explodes "fractally")
-- 15. satellites

function initPickups()
	Pickups.initFrames()
	Pickups.limit = 3
end

function startPickups()
	Pickups.list = {}
	Pickups.delay = 3.2
	Pickups.timer = Pickups.delay
	Pickups.setCurrent(PickupTypes.normal)
	Pickups.count = 15
end



function Pickups.initFrames()
	local img = love.graphics.newImage("img/pickups2.png")
	Pickups.frames = {}
	Pickups.batch = love.graphics.newSpriteBatch(img, 12, "static")
	for j=0,2 do
		for i=0,5 do
			table.insert(Pickups.frames,
				love.graphics.newQuad(i*64, (5+j)*64, 64, 64, img:getWidth(), img:getHeight()))
		end
	end
end

function Pickups.addSingle(startPos)
	if table.getn(Pickups.list) < Pickups.limit then
		Pickups.add(startPos)
	end
end

function Pickups.add(startPos)
	local typ = Formations.getPickups() -- math.random(Pickups.count)
	table.insert(Pickups.list, {
		pos = { x = startPos.x, y = startPos.y },
		dir = { x = 0, y = 1 },
		speed = 300,
		w = 32,
		h = 32,
		entered = false,
		frameNdx = Pickups.ndx2img(typ),
		giveupTimer = 3 + math.random()*8,
		valid = true
		})
end

function Pickups.generate(dt)
	Pickups.timer = Pickups.timer - dt
	if Pickups.timer <= 0 then
		Pickups.timer = Pickups.delay
		Pickups.delay = math.random()*3+1
		-- Pickups.add({x = math.random() * screenSize.x, y = -20})
	end
end

function Pickups.update(dt)
	Pickups.generate(dt)
	for i,v in ipairs(Pickups.list) do
		-- giveup chasing player after some time
		v.giveupTimer = v.giveupTimer - dt

		-- homing
		if Player.p.alive and v.giveupTimer > 0 then
			v.dir = normalize({x = Player.p.pos.x + fgOvr.x - v.pos.x, y = Player.p.pos.y - v.pos.y})
		else
			v.dir = { x = 0, y = 1 }
			v.speed = 160
		end
		v.pos.x = v.pos.x + v.dir.x * v.speed * dt
		v.pos.y = v.pos.y + v.dir.y * v.speed * dt
		if outOfScreen(v.pos, v.w/2, v.h/2) then
			if v.entered then
				v.valid = false
			end
		else
			if not v.entered then
				v.entered = true
			end
		end
	end

	-- purge
	local n = table.getn(Pickups.list)
	for i=1,n do
		local v = Pickups.list[n-i+1]
		if not v.valid then
			table.remove(Pickups.list,n-i+1)
		end
	end
end

function Pickups.draw()
	Pickups.batch:clear()
	for i,v in ipairs(Pickups.list) do
		-- Pickups.batch:add(Pickups.frames[v.frameNdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), 0, 1, 1, 32, 32)
		addToBatch(Pickups.batch,Pickups.frames[v.frameNdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), 0, 1, 1, 32, 32)
	end
	love.graphics.draw(Pickups.batch)
end

function Pickups.setCurrent(pui)
	Pickups.current = pui
	registerPickup(Pickups.current)
end

function Pickups.activate(pu)
	Pickups.setCurrent(Pickups.img2ndx(pu.frameNdx))
	playSound(Sound.pickup, Player.p.pos.x)
	if Pickups.current == PickupTypes.satellites then
		Satellites.respawn()
	else
		Satellites.discard()
	end
end

function Pickups.img2ndx(imgIndex)
	if imgIndex == 1 then
		return  6
	elseif imgIndex == 2 then
		return  3
	elseif imgIndex == 3 then
		return  4
	elseif imgIndex == 4 then
		return  7
	elseif imgIndex == 5 then
		return  11
	elseif imgIndex == 6 then
		return  5
	elseif imgIndex == 7 then
		return  12
	elseif imgIndex == 8 then
		return  10
	elseif imgIndex == 9 then
		return  9
	elseif imgIndex == 10 then
		return  8
	elseif imgIndex == 11 then
		return  2
	elseif imgIndex == 12 then
		return  1
	elseif imgIndex == 13 then
		return  13
	elseif imgIndex == 15 then
		return  15
	elseif imgIndex == 16 then
		return  14
	else
		-- default
		return  1
	end
end

function Pickups.ndx2img(pickupIndex)
	if pickupIndex ==  1 then
		return 12
	elseif pickupIndex ==  2 then
		return 11
	elseif pickupIndex ==  3 then
		return 2
	elseif pickupIndex ==  4 then
		return 3
	elseif pickupIndex ==  5 then
		return 6
	elseif pickupIndex ==  6 then
		return 1
	elseif pickupIndex ==  7 then
		return 4
	elseif pickupIndex ==  8 then
		return 10
	elseif pickupIndex ==  9 then
		return 9
	elseif pickupIndex ==  10 then
		return 8
	elseif pickupIndex ==  11 then
		return 5
	elseif pickupIndex ==  12 then
		return 7
	elseif pickupIndex == 13 then
		return 13
	elseif pickupIndex == 14 then
		return  16
	elseif pickupIndex == 15 then
		return  15
	else
		-- default
		return 1
	end
end

function Pickups.drawCurrent()
	love.graphics.setColor(255,255,255)
	Pickups.batch:clear()
	-- Pickups.batch:add(Pickups.frames[Pickups.ndx2img(Pickups.current)], 24 + Pickups.current * 12, screenSize.y - 24, 0, 1, 1, 32, 32)
	addToBatch(Pickups.batch,Pickups.frames[Pickups.ndx2img(Pickups.current)], 24 + Pickups.current * 12, screenSize.y - 24, 0, 1, 1, 32, 32)
	love.graphics.draw(Pickups.batch)

end
