EnemyTypes = {
	normal = 1,
	snake = 2,
	mega = 3,
	mill = 4,
	bishop = 5,
	kamikaze = 6,
	hawk = 7,
	spiral = 8,
	plant = 9,
	clock = 10,
	swarm = 11,
	falcon = 12,
	chicken = 13
}

BulletTypes = {
	playerlaser = 0,
	sinus = 1,
	cheese = 2,
	card = 3,
	cat = 4,
	sushi = 5,
	subsushi = 6,
	bottle = 7,
	heart = 8,
	flower = 9,
	enemynormal = 10,
	enemylaser = 11,
	enemyplant = 12,
	raspberry = 13
}

PickupTypes = {
	normal = 1,
	sinus = 2,
	cheese = 3,
	card = 4,
	cat = 5,
	sushi = 6,
	bottle = 7,
	heart = 8,
	wind = 9,
	rotate = 10,
	mushroom = 11,
	superspeed = 12,
	invulnerability = 13,
	flower = 14,
	satellites = 15
}

AchievTypes = {
	triggerhappy = 1,
	executioner = 2,
	xenocide = 3,
	ferriswheel = 4,
	trippin = 5,
	gourmet = 6,
	houseware = 7,
	hungry = 8,
	gardener = 9,
	stealthking = 10,
	bullfight = 11,
	dodgemaster = 12,
	piedpiper = 13,
	hardcore = 14
}
