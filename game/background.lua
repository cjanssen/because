function initBg()
	bgImg = love.graphics.newImage("img/bg.png")

	prepareStripes()
	prepareTwinklies()
end

function startBg()
	bgPos = { x= 100, y = 0 }
	fgOvr = { x = 150, y = 0}
	bgSpeed = { x = 0, y = 100 }

	restartTwinklies()
end

function prepareStripes()
	local img = love.graphics.newImage("img/speedstripe2.png")
	Stripes = {}
	Stripes.frames = {}
	Stripes.batch = love.graphics.newSpriteBatch(img,16,"static")
	for i=0,4 do
		table.insert(Stripes.frames, 
			love.graphics.newQuad(i*64, 0, 64, 600, img:getWidth(), img:getHeight()))
	end

	Stripes.list = {}
	stripesVisible = true
end

function addStripe()
	table.insert(Stripes.list, {
		frameIndex = 1,
		frameTimer = 0.05,
		frameDelay = 0.05,
		xpos = math.random() * screenSize.x
	})
end

function prepareTwinklies()
	local img = love.graphics.newImage("img/twinklies.png")
	Twinklies = {}
	Twinklies.frames = {}
	Twinklies.batches = {}

	for i=1,12 do
		table.insert(Twinklies.batches, love.graphics.newSpriteBatch(img,120,"static"))
	end

	for j=0,3 do
		table.insert(Twinklies.frames,{})
		for i=0,1 do
			table.insert(Twinklies.frames[j+1], 
				love.graphics.newQuad(i*32, j*32, 32, 32, img:getWidth(), img:getHeight()))
		end
	end
end

function restartTwinklies()
	for i,v in ipairs(Twinklies.batches) do
		v:clear()
	end

	Twinklies.control = {
		pos = { x = 120, y = 0 },
		speed = 120,
		frameIndexes = {1,1,1},
		frameDelays = {math.pi/12,math.sqrt(2)/12,math.sqrt(5)/12},
		frameTimers = {0,0,0}
	}

	for i=1,60 do
		addTwinklie(math.random()*screenSize.y)
	end
	flipTwinkliesLayers()
end

function addTwinklie(posy)
	local period = math.random(3)
	local posx = math.random()*bgImg:getWidth()*1.2
	posy = posy or (screenSize.y - Twinklies.control.pos.y)
	local twinklieIndex = math.random(table.getn(Twinklies.frames))
	
	addToBatch(Twinklies.batches[period*2-1], Twinklies.frames[twinklieIndex][1], posx, posy)
	addToBatch(Twinklies.batches[period*2], Twinklies.frames[twinklieIndex][2], posx, posy)
end

function drawBg()
	drawVibration(1)
	-- image
	love.graphics.draw(bgImg,math.floor(-bgPos.x),math.floor(bgPos.y))
	if bgPos.y > 0 then
		love.graphics.draw(bgImg,math.floor(-bgPos.x),math.floor(bgPos.y - bgImg:getHeight()))
	end

	drawEndGamePre()

	drawVibration(2)
	drawTwinklies()
end

function flipTwinkliesLayers()
	for i=1,6 do
		local tmp = Twinklies.batches[i]
		Twinklies.batches[i] = Twinklies.batches[i+6]
		Twinklies.batches[i+6] = tmp
	end
end

function drawTwinklies()
	for i=1,3 do
		local layerNdx = i*2 + Twinklies.control.frameIndexes[i] - 2
		love.graphics.draw(Twinklies.batches[layerNdx],
			math.floor(-Twinklies.control.pos.x), math.floor(Twinklies.control.pos.y - screenSize.y))
		love.graphics.draw(Twinklies.batches[layerNdx+6],
			math.floor(-Twinklies.control.pos.x), math.floor(Twinklies.control.pos.y))
	end
end

function drawStripes()
	if stripesVisible then
		-- speed stripes
		Stripes.batch:clear()
		for i,v in ipairs(Stripes.list) do
			-- Stripes.batch:add(Stripes.frames[v.frameIndex], math.floor(v.xpos), 0)
			addToBatch(Stripes.batch,Stripes.frames[v.frameIndex], math.floor(v.xpos), 0)
		end
		love.graphics.draw(Stripes.batch)
	end
end

function updateBg(dt)
	-- horizontal scroll
	bgPos.x = Player.p.pos.x / screenSize.x * (bgImg:getWidth() - screenSize.x)


	-- vertical scroll
	bgPos.y = bgPos.y + bgSpeed.y * dt
	if bgPos.y > screenSize.y then
		bgPos.y = bgPos.y - bgImg:getHeight()
	end

	updateStripes(dt)
	updateTwinklies(dt)
end

function updateStripes(dt)
	-- speed stripes
	local n = table.getn(Stripes.list)
	for i=1,n do
		local v = Stripes.list[n-i+1]
		v.frameTimer = v.frameTimer - dt
		if v.frameTimer <= 0 then
			v.frameTimer = v.frameDelay
			v.frameIndex = v.frameIndex + 1
			if v.frameIndex > table.getn(Stripes.frames) then
				table.remove(Stripes.list,n-i+1)
			end
		end
	end
	if stripesVisible and math.random()<0.1 then
		addStripe()
	end

	-- foreground
	fgOvr.x = Player.p.pos.x / screenSize.x * (bgImg:getWidth() * 1.5 - screenSize.x)
end

function updateTwinklies(dt)
	-- scroll
	Twinklies.control.pos.x = Player.p.pos.x / screenSize.x * (bgImg:getWidth() * 1.2 - screenSize.x)
	Twinklies.control.pos.y = Twinklies.control.pos.y + Twinklies.control.speed * dt
	if Twinklies.control.pos.y >= screenSize.y then
		Twinklies.control.pos.y = Twinklies.control.pos.y - screenSize.y
		flipTwinkliesLayers()
		for i=1,6 do
			Twinklies.batches[i]:clear()
		end
	end

	-- animation
	for i=1,3 do
		Twinklies.control.frameTimers[i] = Twinklies.control.frameTimers[i] - dt
		if Twinklies.control.frameTimers[i] <= 0 then
			Twinklies.control.frameTimers[i] = Twinklies.control.frameTimers[i] + Twinklies.control.frameDelays[i]
			Twinklies.control.frameIndexes[i] = 3 - Twinklies.control.frameIndexes[i]
		end
	end

	-- spawn
	if Twinklies.control.speed > 50 and math.random()<0.2 then
		addTwinklie()
	end
end
--------------------
function initOverlay()
	--overlaz = love.graphics.newImage("img/overlaz.png")
	overlay1 = love.graphics.newImage("img/overlay1.png")
	overlay2 = love.graphics.newImage("img/overlay2.png")
	overlay2_opacity = 1
	min_overlay2_opacity = 0.3
	overlay2_range = 100
end

function startOverlay()
	overlay2_opacity = 1
end

function updateOverlay(dt)
	if Player.p.pos.x < screenSize.x - 200 - overlay2_range then
		overlay2_opacity = 1
		return
	end

	if Player.p.pos.y < screenSize.y - 200 - overlay2_range then
		overlay2_opacity = 1
		return
	end

	if Player.p.pos.x > screenSize.x - 200 and Player.p.pos.y > screenSize.y - 200 then
		overlay2_opacity = min_overlay2_opacity
		return
	end

	local ox = (screenSize.x - 200) - Player.p.pos.x
	local oy = (screenSize.y - 200) - Player.p.pos.y
	local om = ox
	if oy>ox then om = oy end
	overlay2_opacity = (1 - min_overlay2_opacity) * (om/overlay2_range) + min_overlay2_opacity
end

function drawOverlay()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(overlay1, 0, 0)
	love.graphics.setColor(255,255,255,overlay2_opacity*255)
	love.graphics.draw(overlay2, 0, 0)
	love.graphics.setColor(255,255,255)
end

---------------------
function initBlackOverlay()
	blackOverlay = {
		opacity = 1,
		goal = 1,
		rate = 0.7
	}
end

function fadeToBlack()
	blackOverlay.goal = 1
end

function fadeFromBlack()
	blackOverlay.goal = 0
end

function updateBlackOverlay(dt)
	if blackOverlay.opacity < blackOverlay.goal then
		blackOverlay.opacity = increaseExponential(dt, blackOverlay.opacity, blackOverlay.rate)
	elseif blackOverlay.opacity > blackOverlay.goal then
		blackOverlay.opacity = decreaseExponential(dt, blackOverlay.opacity, blackOverlay.rate)
	end
end

function drawBlackOverlay()
	if blackOverlay.opacity > 0 then
		love.graphics.setColor(0,0,0,blackOverlay.opacity*255)
		love.graphics.rectangle("fill",0,0,screenSize.x, screenSize.y)
		love.graphics.setColor(255,255,255)
	end
end
