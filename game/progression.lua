Progression = {}
function initProgression()
	Progression = {
		opacity = 0,
		waveCount = 1,
		totalWaves = 37,
		-- totalWaves = 2,
		percentage = 0,

		timer = 0,
		startShowTime = 2.5,
		endShowTime = 0.5,
		totalShowTime = 6,
		
		counters = {}
	}

	-- difficulty at 36 is 10, at 37 is 5.6, that's why there's 37 waves

end

function startProgression()
	Progression.opacity = 0
	Progression.timer = 0
	Progression.counters = {0,0,0,0,0,0,0}
	difficulty.value = 1
	Progression.waveCount = 1
end

function progressionStep()
	if not Player.p.alive or gameFinished then return end


	Progression.waveCount = Progression.waveCount + 1
	registerSectorReach(Progression.waveCount)
	if Progression.waveCount >= Progression.totalWaves and Player.p.alive then
		endGame()
		return
	end

	local progAmount = 1.5
	local cycle = 3
	local ndx = 1
	local cont = false
	repeat
		cont = false
		Progression.counters[ndx] = Progression.counters[ndx] + progAmount
		if Progression.counters[ndx] >= progAmount * cycle and ndx < table.getn(Progression.counters) then
			Progression.counters[ndx] = 0
			ndx = ndx + 1
			progAmount = progAmount * math.pow(2,1/4)
			cont = true
		end
	until not cont

	difficulty.value = 1
	for i,v in ipairs(Progression.counters) do
		difficulty.value = difficulty.value + v
	end

	if difficulty.value > 10 then difficulty.value = 10 end
	startShowSector()
end

function updateProgression(dt)
	if Progression.timer > 0 then
		Progression.timer = Progression.timer - dt
		if Progression.timer <= 0 then
			Progression.opacity = 0
		elseif Progression.timer < Progression.endShowTime then
			Progression.opacity = decreaseExponential(dt, Progression.opacity, 0.95)
		elseif Progression.timer < Progression.startShowTime then
			Progression.opacity = increaseExponential(dt, Progression.opacity, 0.95)
		end
	end
end

function startShowSector()
	Progression.timer = Progression.totalShowTime
end

function countProgression()
	local frac = 0
	if Formations.delay > 0 then
		frac = (Formations.delay - Formations.timer) / Formations.delay
	end
	Progression.percentage = math.max(100.0 * (Progression.waveCount-1+frac)/Progression.totalWaves,0)
end

function drawProgress()
	if not newgameScreen.firstTime then
		printShadowedText(string.format("PROGRESS %.2f%s",Progression.percentage,"%"),
			math.floor(screenSize.x/2), 
			math.floor(screenSize.y/4+60), 1, true )
	end
end

function drawZoneText()
	if Progression.opacity > 0 then
		printShadowedText(string.format("SECTOR %i", Progression.waveCount),
			math.floor(screenSize.x/2), math.floor(screenSize.y/4), Progression.opacity, true)
	end
end


