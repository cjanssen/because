EnemyFactory = {}

function initEnemyFactory()
end

function EnemyFactory.spawnEnemy(enemyType, startPos, extradata)
	if enemyType == 1 then
		EnemyFactory.addNormal(startPos)
	elseif enemyType == 2 then
		EnemyFactory.addSnake(startPos, extradata)
	elseif enemyType == 3 then
		EnemyFactory.addMega(startPos)
	elseif enemyType == 4 then
		EnemyFactory.addMill(startPos)
	elseif enemyType == 5 then
		EnemyFactory.addBishop(startPos)
	elseif enemyType == 6 then
		EnemyFactory.addKamikaze(startPos)
	elseif enemyType == 7 then
		EnemyFactory.addHawk(startPos)
	elseif enemyType == 8 then
		EnemyFactory.addSpiral(startPos)
	elseif enemyType == 9 then
		EnemyFactory.addPlant(startPos)
	elseif enemyType == 10 then
		EnemyFactory.addClock(startPos, extradata)
	elseif enemyType == 11 then
		EnemyFactory.addFalcon(startPos)
	elseif enemyType == 12 then
		EnemyFactory.addSwarm(startPos)
	end
end

function EnemyFactory.addNormal(startPos)
	table.insert(Enemies.list, {
		eType = EnemyTypes.normal,
		pos = {x = startPos.x, y = startPos.y},
		dir = { x = 0, y = 1},
		speed = 150,
		w = 24,
		h = 24,
		shootTimer = math.random(),
		shootDelay = params.normalEnemyShootDelay(),
		spriteNdx = 1,
		alive = true,
		angle = 0
		})
end

function EnemyFactory.prepareSnake(cols, rows)
	local extradata = {
		pos = {0,0},
		oscS = params.snakeEnemyOscillationSpeed(),
		oscA = params.snakeEnemyOscillationAmplitude(),
		periods = {cols, rows}
	}
	return extradata

end

function EnemyFactory.addSnake(startPos, extradata)
	table.insert(Enemies.list, {
		eType = EnemyTypes.snake,
		pos = {x = startPos.x, y = startPos.y},
		centerX = startPos.x,
		dir = { x = 0, y = 1},
		speed = 150,
		w = 24,
		h = 24,
		shootTimer = math.random(),
		shootDelay = params.snakeEnemyShootDelay(),
		spriteNdx = 3,
		oscSpeed = extradata.oscS,
		oscAmp = extradata.oscA,
		oscPhase = -math.pi * (extradata.pos[1]/extradata.periods[1] + extradata.pos[2]/extradata.periods[2]),
		alive = true,
		angle = 0
		})
end

function EnemyFactory.addMega(startPos)
	table.insert(Enemies.list, {
		eType = EnemyTypes.mega,
		pos = {x = startPos.x, y = startPos.y},
		dir = { x = 0, y = 1},
		speed = 150,
		w = 48,
		h = 48,
		shootTimer = math.random(),
		shootDelay = params.megaEnemyShootDelay(),
		shootCount = math.random(4)-1,
		shootLoop = params.megaEnemyShootLoop(),

		laserAmount = params.megaEnemyLaserAmount(),
		laserEnabled = params.megaEnemyLaserEnabled(),
		homingEnabled = params.megaEnemyHomingEnabled(),

		spriteNdx = 2,
		alive = true,
		angle = 0
		})
end

function EnemyFactory.addMill(startPos)
	local rotcount = params.millEnemyRotationCount()
	table.insert(Enemies.list, {
		eType = EnemyTypes.mill,
		pos = {x = startPos.x, y = startPos.y},
		dir = { x = 0, y = 1},
		speed = 150,
		w = 48,
		h = 48,

		laserEnabled = params.millEnemyLaserEnabled(),

		shootState = math.random(rotcount) - 1,
		rotState = rotcount,
		shootTimer = math.random(),
		shootDelay = params.millEnemyShootDelay(),
		shootFirstDelay = params.millEnemyFirstDelay(),
		shootSecondDelay = params.millEnemySecondDelay(),
		
		spriteNdx = 5,
		angle = 0,
		rotTime = params.millEnemyRotTime(),
		rotating = false,
		alive = true,
		})
end

function EnemyFactory.addBishop(startPos)
	local sign = 1
	if startPos.x >= bgImg:getWidth()*0.75 then
		sign = -1
	end
	table.insert(Enemies.list, {
		eType = EnemyTypes.bishop,
		pos = {x = startPos.x, y = startPos.y},
		dir = { x = math.sin(sign * 30*math.pi/180)*2, y = 1},
		vel = { x = 0, y = 1},
		speed = 150,
		w = 24,
		h = 24,
		shootTimer = math.random(),
		shootDelay = params.bishopEnemyShootDelay(),

		bulletCount = params.bishopEnemyBulletCount(),
		bulletSeparation = params.bishopEnemyBulletSeparation(),
		
		spriteNdx = 4,
		alive = true,
		angle = -sign * 60 * math.pi / 180
		})
end


function EnemyFactory.addKamikaze(startPos)
	local spd = 220 * params.kamikazeEnemySpeedup()
	-- choose a new starting pos
	if math.random()<0.25 then
		-- in the border
		startPos.x = -16
		if math.random() < 0.5 then startPos.x = bgImg:getWidth()*1.5 + 16 end
		startPos.y = math.random() * screenSize.y
	end
	-- configure explosion time
	local diff = {x = Player.p.pos.x + fgOvr.x - startPos.x, y = Player.p.pos.y - startPos.y }
	local dist = math.sqrt(diff.x*diff.x + diff.y*diff.y)
	local time = dist / spd * params.kamikazeEnemyExplosionDelay()

	-- rotation
	local sign = 1
	if math.random() < 0.5 then sign = -1 end
	local ang = math.random() * math.pi * 2
	if Player.p.alive then
		local dir = {x = Player.p.pos.x + fgOvr.x - startPos.x, y = Player.p.pos.y - startPos.y }
		ang = math.atan2(dir.y,dir.x)
	end

	table.insert(Enemies.list, {
		eType = EnemyTypes.kamikaze,
		pos = {x = startPos.x, y = startPos.y},
		dir = { x = math.cos(ang), y = math.sin(ang)},
		speed = spd,
		w = 24,
		h = 24,
		shootTimer = time,
		shootDelay = time,
		spriteNdx = 7,
		alive = true,
		angle = 0,
		maxSteer = params.kamikazeEnemyMaxSteer(),
		rotating = false
	})
end


function EnemyFactory.addHawk(startPos)
	local sign = 1
	if startPos.x >= bgImg:getWidth()*0.75 then
		sign = -1
	end
	local ang = sign*60*math.pi/180
	table.insert(Enemies.list, {
		eType = EnemyTypes.hawk,
		pos = {x = startPos.x, y = startPos.y},
		dir = { x = math.sin(ang)/math.cos(ang), y = 1},
		speed = 150,
		w = 48,
		h = 48,
		shootTimer = math.random() * params.hawkEnemyShootDelay(),
		shootDelay = params.hawkEnemyShootDelay(),
		spriteNdx = 8,
		alive = true,
		angle = -ang*75/60
		})
end


function EnemyFactory.addChickenGroup(startPos)
	local phase = math.random()*math.pi*2
	local nb = params.chickenGroupSize()
	for i=0,nb-1 do
		EnemyFactory.addChicken(startPos, phase, i, nb)
	end
end

function EnemyFactory.addChicken(startPos, phase, ndx, total)
	table.insert(Enemies.list, {
		eType = EnemyTypes.chicken,
		pos = { x = startPos.x , y = startPos.y },
		speed = 350 + math.random(250),
		dir = { x = math.cos(ndx/total*math.pi*2), y = -math.sin(ndx/total*math.pi*2)},
		w = 24,
		h = 24,
		spriteNdx = 9,
		cannotShoot = true,
		angle = phase,

		alive = true,
		scale = 0.8,
		slowdown = params.chickenEnemySlowdown(),
		disapThres = params.chickenEnemyThreshold(),
		drift = math.random() * math.pi * 4
	})
end

function EnemyFactory.addSpiral(startPos)
	local ang = math.random() * math.pi*2
	table.insert(Enemies.list, {
		eType = EnemyTypes.spiral,
		pos = { x = startPos.x, y = startPos.y},
		dir = {x = math.cos(ang), y = math.sin(ang)},
		speed = params.spiralEnemySpeed(),
		w = 24,
		h = 24,
		cannotShoot = false,
		shootTimer = math.random(),
		shootDelay = params.spiralEnemyShootDelay(),
		spriteNdx = 10,
		alive = true,
		angle = 0,
		attraction = params.spiralEnemyAttraction()
		})
end

function EnemyFactory.addPlant(startPos)
	table.insert(Enemies.list, {
		eType = EnemyTypes.plant,
		pos = {x = startPos.x, y = startPos.y},
		dir = { x = 0, y = 1},
		speed = 150,
		w = 48,
		h = 48,
		shootTimer = 0.5 + math.random()*0.5,
		shootDelay = params.plantEnemyShootDelay(),
		spriteNdx = 11,
		angle = 0,
		alive = true,
		})
end

function EnemyFactory.prepareClockFormation(sp,cols,rows)
	local extradata = {
		fControl = { 
			seqLen = params.clockEnemySeqLen(), 
			rowCount = params.clockEnemyRowCount(), 
			steps={} },
		sep = params.clockEnemySep(),
		initAdv = params.clockEnemyInitialAdv(),
		startPos = sp,
		pos = {0,0}
	}
	return extradata
end

function EnemyFactory.addClock(startPos, extradata)
	local spd = 150
	local sspd = 100
	-- override startPos
	startPos = extradata.startPos
	table.insert(Enemies.list, {
		eType = EnemyTypes.clock,
		pos = {x = startPos.x + extradata.pos[1]*extradata.sep, y = startPos.y - extradata.pos[2]*extradata.sep},
		dir = { x = 0, y = 1 },
		speed = spd,
		scrollSpeed = sspd,
		w = 24,
		h = 24,

		turnState = 0,
		dirState = 0,
		seq = extradata.pos[1],
		advanceTime = ((extradata.fControl.seqLen-extradata.pos[1]+1)+extradata.initAdv+extradata.pos[2])*extradata.sep / spd,
		formationControl = extradata.fControl,
		separation = extradata.sep,
		displaceCenter = true,
		
		shootTimer = math.random(),
		shootDelay = params.clockEnemyShootDelay(),
		spriteNdx = 21,
		angle = 0,
		alive = true,
		})
end

function EnemyFactory.addSwarm(startPos)
	local ang = math.random() * math.pi * 2
	local bs = params.swarmEnemyBaseSpeed()
	local vs = params.swarmEnemyVariableSpeed()
	table.insert(Enemies.list, {
		eType = EnemyTypes.swarm,
		pos = {x = startPos.x, y = startPos.y},
		dir = { x = 0, y = 1},
		speed = 150,
		w = 24,
		h = 24,

		shootTimer = math.random(),
		shootDelay = params.swarmEnemyShootDelay(),

		spriteNdx = 19,
		angle = 0,
		alive = true,

		switchSign = -1,
		currentFactor = 1,
		switchFactor = params.swarmEnemySwitchFactor(),
		origSpeed = 150,
		baseSpeed = bs,
		varSpeed = vs,
		newSpeed = bs + math.random()*vs,
		origDir = { x = 0, y = 1 },
		newDir = { x = math.cos(ang), y = math.sin(ang) },
		switchTimer = params.swarmEnemySwitchTimer(),
		switchDelay = params.swarmEnemySwitchDelay(),
		switchDelayDown = params.swarmEnemySwitchDelayDown(),
		switchDelayUp = params.swarmEnemySwitchDelayUp()
		})
end

function EnemyFactory.addFalcon(startPos)
	table.insert(Enemies.list, {
		eType = EnemyTypes.falcon,
		pos = {x = startPos.x, y = startPos.y},
		dir = { x = 0, y = 1},
		speed = 200,
		w = 48,
		h = 48,

		shootState = 0,
		advanceTime = 	params.falconEnemyAdvanceTime(),
		downDelay = params.falconEnemyDownDelay(),
		rotDelay = params.falconEnemyRotDelay(),
		shootingDelay = params.falconEnemyShootingDelay(),
		separation = params.falconEnemySeparation(),
		scrollSpeed = 100,
		shootSpeed = 0,
		moveSpeed = 200,

		knockbackRate = params.falconEnemyKnockbackRate(),
		knockbackSpeed = params.falconEnemyKnockbackSpeed(),

		shootTimer = math.random()*0.2,
		shootDelay = params.falconEnemyShootDelay(),
		mgDelay = params.falconEnemyMgDelay(),
		normalBulletDelay = params.falconEnemyNormalDelay(),
		spriteNdx = 20,
		angle = 0,
		alive = true,
	})
end