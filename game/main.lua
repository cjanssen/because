
function include( filename )
	love.filesystem.load( filename )()
end

function love.resize(w,h)
	ratios = {w/screenSize.x,h/screenSize.y}
	if ratios[1]<ratios[2] then
		ratios[2] = ratios[1]
		translation = {0, -(screenSize.y*ratios[1] - h) * 0.5}
		translation[1] = 0
	else
		ratios[1] = ratios[2]
		translation = {-(screenSize.x*ratios[2] - w) * 0.5, 0}
	end
	scale = ratios
end

function love.load()
	screenSize = { x = 800, y = 600 }
	include("constants.lua")
	include("player.lua")
	include("bullets.lua")
	include("enemyfactory.lua")
	include("enemies.lua")
	include("background.lua")
	include("explosions.lua")
	include("pickups.lua")
	include("extras.lua")
	include("vibration.lua")
	include("highscore.lua")
	include("endgame.lua")
	include("progression.lua")
	include("wind.lua")
	include("mushroom.lua")
	include("lifespawner.lua")
	include("helpers.lua")
	include("formations.lua")
	include("difficulty.lua")
	include("options.lua")
	include("sound.lua")
	include("achievements.lua")

	love.mouse.setVisible(not isFullscreen)
	initialize()
	newGame()
end

function initialize()
	font = love.graphics.newImageFont("img/letters.png","ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.%:,+- /*=!?'",1)
	love.graphics.setFont(font)
	scale = {1,1}
	translation = {0,0}
	initDifficulty()
	initFormations()
	initProgression()
	initBg()
	initPlayer()
	initBullets()
	initEnemyFactory()
	initEnemies()
	initExplo()
	initPickups()
	initSatellites()
	initMushroom()
	initWind()
	initOverlay()
	initLifeMeter()
	prepareNewGameScreen()
	loadJoystick()
	initBlackOverlay()
	showBeginning()
	initSound()
	fadeFromBlack()
	startCredits()
	initAchievements()
	initOptions()
	retrieveData()
	initEndGame()
	initVibration()
	initScoreDisplay()
	gameState = 1
end

function newGame()
	startBg()
	startPlayer()
	Player.spawn()
	startBullets()
	startEnemies()
	startExplo()
	startPickups()
	startSatellites()
	startLifeSpawner()
	startNewGameScreen()
	startMushroom()
	startWind()
	startFormations()
	startProgression()
	startOverlay()
	startOptions()
	initBlackOverlay()
	fadeFromBlack()
	newgamePending = false
	prepareEndGame()
	disableMushroomSound()
	resetMusicPlaylist()
	startVibration()
	startScoreDisplay()
	startAchievements()
	if newgameScreen.firstTime then
		gameState = 1
	else
		gameState = 2
		playIngameMusic()
		registerStartGame()
	end
end

function startMenu()
	registerResetGame()
	newGame()
	gameState = 1
	playIntroMusic()
	Player.p.lives = 0
	Player.p.alive = false
	newgameScreen.timer = 0.001
end

function showBeginning()
	xbergImg = love.graphics.newImage("img/madeinxberg.png")
	xbergTimer = 2
end

function startCredits()
	creditsTimer = 3.5
	creditsOpacity = 0
end

function scheduleNewGame()
	fadeToBlack()
	newgamePending = true
end

function loadJoystick()
	if loveVersion == 9 then
		if not joystickOpen then
			local jc = love.joystick.getJoystickCount()
			if jc == 0 then return end
			local joys = love.joystick.getJoysticks()
			-- try from 0 to 4
			for i,v in ipairs(joys) do
				if v:getButtonCount()>0 then
					joystickOpen = v
					if not joystickOpen:isGamepad() and (string.upper(joystickOpen:getName()) == "XEQX GAMEPAD SL-6556-BK") then
						joystickSpecial = true
					end
					return
				end
			end
			joystickOpen = false
		end
	elseif loveVersion == 8 then
	 	if not joystickOpen then
	 		-- try from 0 to 4
	 		for i=1,4 do
				if love.joystick.open(i) and love.joystick.getNumButtons(i)>0 then
	 				joystickOpen = i
	 				if (string.upper(love.joystick.getName(joystickOpen)) == "XEQX GAMEPAD SL-6556-BK") then
						joystickSpecial = true
					end
	 				return
	 			end
	 		end
	 		joystickOpen = false
		end
	end
end

function startNewGameScreen()
	newgameScreen.timer = 0
	if newgameScreen.firstTime then
		Player.p.alive = false
		Player.p.lives = 0
	else
		newgameScreen.show = false
	end
end

function prepareNewGameScreen()
	newgameScreen = {
		show = true,
		timer = 0,
		delay = 2.2,
		firstTime = true
	}
	highscoreTable = {}
end

function promptNewgame()
	newgameScreen.timer = newgameScreen.delay
	if Player.p.lives > 0 then
		newgameScreen.timer = newgameScreen.timer / 4
	else
		registerFailedGame()
		saveScore()
	end
end

function love.draw()
	adaptView()

	if xbergTimer > 0 then
		love.graphics.setColor(255,255,255)
		love.graphics.draw(xbergImg,screenSize.x/2,screenSize.y/2,0,1,1,xbergImg:getWidth()/2,xbergImg:getHeight()/2)
		drawBlackOverlay()
		love.graphics.pop()
		return
	end

	drawMushroom()

	drawBg()
	drawVibration(3)
	drawLifeSpawner()
	Bullets.draw()
	Enemies.draw()
	Satellites.draw()
	Player.draw()
	Explo.draw()
	Pickups.draw()
	drawStripes()
	undrawVibration()
	undrawMushroom()

	drawBlackOverlay()

	if pauseScreen.paused then
		drawOptions()
	elseif newgameScreen.show then

		if newgameScreen.firstTime then
			printShadowedText("PRESS ACTION TO PLAY", screenSize.x/2, screenSize.y/4+40, 1, true)
		else
			printShadowedText("GAEM OVRE", screenSize.x/2, screenSize.y/4+40, 1, true)
		end
		drawScores()
	end

	drawAchievements()
	drawAchievementsTmp()

	drawOverlay()
	Pickups.drawCurrent()
	if gameState ~= 1 then
		drawLifeMeter()
	end
	drawZoneText()
	drawCredits()

	if gameFinished then
		drawEndgame()
	end

	love.graphics.pop()
end

function adaptView()
	love.graphics.push()
	love.graphics.translate(translation[1],translation[2])
	love.graphics.scale(scale[1],scale[2])
	love.graphics.setScissor(translation[1],translation[2],screenSize.x*scale[1],screenSize.y*scale[2])
	love.graphics.setColor(255,255,255)
end

function drawCredits()
	if creditsOpacity > 0 then
		printShadowedText("ART: MICHAEL HUSSINGER\nCODE: CHRISTIAAN JANSSEN", 16, 36,creditsOpacity)
		printShadowedText("ESC: OPTIONS",16,78,creditsOpacity)
	end
end

function updateCredits(dt)
	creditsTimer = creditsTimer - dt
	if creditsTimer > 0.3 and creditsTimer < 2 then
		creditsOpacity = increaseExponential(dt,creditsOpacity,0.95)
	elseif creditsTimer < 0.3 then
		creditsOpacity = decreaseExponential(dt,creditsOpacity,0.95)
	end
end

function love.update(dt)
	-- skip slow frames (below 10fps)
	if dt>1/10 then
		return
	end

	if xbergTimer > 0 then
		xbergTimer = xbergTimer - dt
		if xbergTimer <= 0 then
			fadeFromBlack()
			playIntroMusic()
		elseif xbergTimer < 0.3 then
			fadeToBlack()
		end
		updateBlackOverlay(dt)
		return
	end

	updateDifficulty(dt)

	updateMushroom(dt)
	updateWind(dt)
	updateBlackOverlay(dt)
	updateOverlay(dt)
	updateCredits(dt)

	if not pauseScreen.paused then
		updateVibration(dt)
		updateBg(dt)
		Enemies.update(dt)
		Player.update(dt)
		Satellites.update(dt)
		Bullets.update(dt)
		Explo.update(dt)
		Pickups.update(dt)
		updateLifeSpawner(dt)
		updateFormations(dt)
		updateProgression(dt)
		updateMushroomSound(dt)
		updateMusic(dt)
		updateAchievements(dt)

		if gameFinished then
			updateEndgame(dt)
		end

		if newgameScreen.show then
			updateScoreDisplay(dt)
		end
	else
		updateOptions(dt)
	end

	-- newgame
	if newgameScreen.timer > 0 then
		newgameScreen.timer = newgameScreen.timer - dt
		if newgameScreen.timer <= 0 then
			if Player.p.lives > 0 then
				Player.spawn()
			else
				registerResetGame()
				newgameScreen.show = true
				playIntroMusic()
				gameState = 1
			end
		end
	end
	if newgamePending and blackOverlay.opacity >= 1 then
		newgameScreen.firstTime = false
		registerResetGame()
		newGame()
	end

end

function love.quit()
	storeData()
end

function love.joystickpressed( joystick, button )
	if retrigger then return end
	retrigger = true

	if xbergTimer > 0 then return end

	if not joystickOpen then
		loadJoystick()
		if not joystickOpen then return end
	end

	-- newgame from joystick
	local nb = 0
	if loveVersion == 9 then
		nb = joystickOpen:getButtonCount()
	elseif loveVersion == 8 then
		nb = love.joystick.getNumButtons( joystickOpen )
	end

	local pausePressed = false

	if loveVersion == 9 then
		if joystickOpen:isGamepad() then
			pausePressed = joystickOpen:isGamepadDown("start","back","guide")
		else
		 	pausePressed = (button > 3 and button < 10) or (nb>14 and (button == 0 or button == 3))
		end
	else
	 	pausePressed = (button > 3 and button < 10) or (nb>14 and (button == 0 or button == 3))
	end

	if joystickSpecial then
		pausePressed = (button == 9 or button == 10)
	end

	if pausePressed then
		togglePause()
	elseif pauseScreen.paused then
		optionsJoystickPressed()
	elseif newgameScreen.show or EndGameOpacity == 1 then
		scheduleNewGame()
	end
end

function love.joystickreleased(joystick, button)
	retrigger = false
end

function love.keypressed(key)
	if xbergTimer > 0 then return end

	if key=="f4" then
		love.event.push("quit")
		return
	end

	if key=="p" or key == "escape" or key=="f1" then
		togglePause()
	end

	if key == "f2" then
		toggleFullscreen()
	end

	if key == "f3" then
		toggleSound()
	end

	if pauseScreen.paused then
		optionsKeypressed(key)
		return
	end

	if newgameScreen.show or EndGameOpacity == 1 then
		if love.keyboard.isDown(" ") or
			love.keyboard.isDown("lctrl") or
			love.keyboard.isDown("rctrl") or
			love.keyboard.isDown("lshift") or
			love.keyboard.isDown("rshift") then
			scheduleNewGame()
		end
	end

	if key=="return" then
		if newgameScreen.show or EndGameOpacity == 1 then
			scheduleNewGame()
		end
	end

end
