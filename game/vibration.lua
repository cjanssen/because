function initVibration()
	vibration = {}
	vibration.value = 0
	vibration.min = 0.3
	vibration.x = 0
	vibration.y = 0
end

function startVibration()
	initVibration()
end

function kickVibration(amount)
	vibration.value = vibration.value + amount
end

function updateVibration(dt)
	vibration.value = decreaseExponential(dt, vibration.value, 0.92)
end

function drawVibration(layer)
	if pauseScreen.paused then return end
	if vibration.value <= vibration.min then return end
	if layer ~= 1 then
		love.graphics.pop()
	end

	if layer == 1 then
		vibration.x = math.random()*2-1
		vibration.y = math.random()*2-1
	end

	-- local amp = math.pow(2,layer)
	local amp = 3

	love.graphics.push()
	love.graphics.translate(vibration.x * vibration.value * amp, vibration.y * vibration.value * amp)
end

function undrawVibration()
	if pauseScreen.paused then return end
	if vibration.value > vibration.min then
		love.graphics.pop()
	end
end