function initOptions()
	pauseScreen = {
		paused = false,
		timer = 0,
		delay = 1,

		pageNames = {
			main = 1,
			highscores = 2,
			delete = 3,
			credits = 4,
			restart = 5,
			exit = 6,
			achiev1 = 7,
			achiev2 = 8
		},

		optionTexts = {{
			"GAMEPAD: NONE", 
			"DISPLAY: WINDOWED",
			"SOUND: ON",
			"SHOW HIGHSCORES",
			"DELETE HIGHSCORES",
			"CREDITS",
			"RESTART",
			"EXIT",
			"CONTINUE"},
			{"NEXT PAGE","BACK"},
			{"CONFIRM DELETE","CANCEL"},
			{"BACK"},
			{"CONFIRM RESTART","BACK"},
			{"CONFIRM EXIT","BACK"},
			{"NEXT PAGE","BACK"},
			{"NEXT PAGE","BACK"}
			 },

		creditText = {
			"'BECAUSE THIS IS LITERALLY THE BEST WE COULD COME UP WITH: RETRIBUTION'",
			"STARTED AT BERLIN MINI GAME JAM JANUARY 2014, MADE WITH LOVE2D",
			" ",
			"GRAPHICS: MICHAEL HUSSINGER  HTTP://WWW.ELECTRIC-ZOMBIE.COM",
			"CODE / SFX: CHRISTIAAN JANSSEN  HTTP://CHRISJANGAMES.WORDPRESS.COM",
			" ",
			"'SAVAGE STEEL FUN CLUB' BY ROLEMUSIC (CC-BY)  HTTP://ROLEMUSIC.SAWSQUARENOISE.COM",
			"'EN CROISIERE' BY JUANITOS (CC-BY)  HTTP://JUANITOS.NET",
			"'OKIIROBO NAVIGATION SYSTEM' BY HENRY HOMESWEET (CC-BY)  HTTP://HENRYHOMESWEET.CO.UK",
			"'CHIPS GOT KICKS' BY ROCCOW (CC-BY) HTTP://ROCCOW.BANDCAMP.COM"
		},
		currentOption = {9,1,2,1,2,2,1,1},
		currentPage = 1,
		currentHiPage = 1,
		soundIsOn = true
	}
	optionsReadFullscreen()
	reloadControlNames()
end

function startOptions()
	pauseScreen.paused = false
end

function togglePause()
	pauseScreen.paused = not pauseScreen.paused
	if pauseScreen.paused then
		reloadControlNames()
		playPauseMusic(true)
		pauseScreen.currentPage = pauseScreen.pageNames.main
	else
		if gameState == 1 then
			playIntroMusic(true)
		elseif gameState == 3 then
			playEndMusic(true)
		else
			playIngameMusic(true)
		end
	end
end

function drawOptions()

	local incy = font:getHeight()*1.6
	local iy = screenSize.y / 4 + incy*2
	local ix = screenSize.x / 2
	if pauseScreen.timer < pauseScreen.delay / 2 then
		printShadowedText("PAUSED", ix, iy, 1, true)
	end

	local baseInc = 2
	if pauseScreen.currentPage == pauseScreen.pageNames.main then
		baseInc = 2
		printShadowedText("MOVE: WASD / CURSORS", ix, iy + incy,0.5,true)
		printShadowedText("SHOOT: CTRL / SHIFT / SPACE", ix, iy+(incy*2),0.5,true)
	elseif pauseScreen.currentPage == pauseScreen.pageNames.highscores then
		baseInc = 10
		drawProgress()
		drawHighscores()
	elseif pauseScreen.currentPage == pauseScreen.pageNames.delete then
		baseInc = 2
		printShadowedText("DELETE HIGHSCORES / ACHIEVEMENTS?",ix,iy+incy+incy,0.8,true)
	elseif pauseScreen.currentPage == pauseScreen.pageNames.credits then
		baseInc = 10
		for i,v in ipairs(pauseScreen.creditText) do
			printShadowedText(v, 80,iy + incy*i,0.8,false)
		end
	elseif pauseScreen.currentPage == pauseScreen.pageNames.restart then
		baseInc = 2
		printShadowedText("RESTART GAME?", ix, iy+incy+incy,0.8,true)
	elseif pauseScreen.currentPage == pauseScreen.pageNames.exit then
		baseInc = 2
		printShadowedText("EXIT GAME?", ix, iy+incy+incy,0.8,true)
	elseif pauseScreen.currentPage == pauseScreen.pageNames.achiev1 then
		baseInc = 10
		printAchievementsTable(1)
	elseif pauseScreen.currentPage == pauseScreen.pageNames.achiev2 then
		baseInc = 10
		printAchievementsTable(2)
	end

	for i=1,table.getn(pauseScreen.optionTexts[pauseScreen.currentPage]) do
		printShadowedText(pauseScreen.optionTexts[pauseScreen.currentPage][i], ix,iy + incy*(baseInc+1+i),1,true)
	end

	love.graphics.setColor(255,255,255)
	local bw = font:getWidth(pauseScreen.optionTexts[pauseScreen.currentPage][pauseScreen.currentOption[pauseScreen.currentPage]])
	love.graphics.rectangle("line",math.floor(ix-bw/2-incy*0.25),
		math.floor(iy+incy*(baseInc+0.5+pauseScreen.currentOption[pauseScreen.currentPage])),
		math.ceil(bw+incy*0.5),math.ceil(incy))
end

function optionsKeypressed(key)
	-- back
	if key == "up" or key == "left" or
		key == "w" or key == "a"
		-- or mouse or joystick
		then
			optionsUpPressed()
		end

	-- forth
	if key == "down" or key == "right" or
		key == "s" or key == "d"
		-- or mouse or joystick
		then
			optionsDownPressed()
		end

	-- select
	if key == "return" or key == " " or 
		key == "lctrl" or key == "lshift" or 
		key == "rctrl" or key == "rshift"
		then
		optionsActionPressed()
	end
end

function optionsUpPressed()
	local nops = table.getn(pauseScreen.optionTexts[pauseScreen.currentPage])
	pauseScreen.currentOption[pauseScreen.currentPage] = 
		((pauseScreen.currentOption[pauseScreen.currentPage] + nops - 2) % nops) + 1
end

function optionsDownPressed()
	pauseScreen.currentOption[pauseScreen.currentPage] = 
		(pauseScreen.currentOption[pauseScreen.currentPage] % table.getn(pauseScreen.optionTexts[pauseScreen.currentPage])) + 1

end

function optionsJoystickPressed()
	-- special cases for detected gamepads
	if loveVersion == 9 and joystickOpen and joystickOpen:isGamepad() then
		if joystickOpen:isGamepadDown("dpup","dpleft") then
			optionsUpPressed()
			return
		end
		if joystickOpen:isGamepadDown("dpdown","dpright") then
			optionsDownPressed()
			return
		end
	end
	optionsActionPressed()
end

function optionsActionPressed()
	if pauseScreen.currentPage == pauseScreen.pageNames.main then
		if pauseScreen.currentOption[1] == 1 then
			selectNextGamepad()
		elseif pauseScreen.currentOption[1] == 2 then
	 		toggleFullscreen()
		elseif pauseScreen.currentOption[1] == 3 then
			toggleSound()
		elseif pauseScreen.currentOption[1] == 4 then
			if pauseScreen.currentHiPage == 1 then
				pauseScreen.currentPage = pauseScreen.pageNames.highscores
			elseif pauseScreen.currentHiPage == 2 then
				pauseScreen.currentPage = pauseScreen.pageNames.achiev1
			else
				pauseScreen.currentPage = pauseScreen.pageNames.achiev2
			end
			pauseScreen.currentOption[pauseScreen.currentPage]=1
			countProgression()
		elseif pauseScreen.currentOption[1] == 5 then
			pauseScreen.currentPage = pauseScreen.pageNames.delete
			pauseScreen.currentOption[pauseScreen.currentPage]=2
		elseif pauseScreen.currentOption[1] == 6 then
			pauseScreen.currentPage = pauseScreen.pageNames.credits
		elseif pauseScreen.currentOption[1] == 7 then
			pauseScreen.currentPage = pauseScreen.pageNames.restart
			pauseScreen.currentOption[pauseScreen.currentPage] = 2
		elseif pauseScreen.currentOption[1] == 8 then
			pauseScreen.currentPage = pauseScreen.pageNames.exit
			pauseScreen.currentOption[pauseScreen.currentPage] = 2
		elseif pauseScreen.currentOption[1] == 9 then
			togglePause()
			return
		end
	elseif pauseScreen.currentPage == pauseScreen.pageNames.highscores then
		if pauseScreen.currentOption[pauseScreen.currentPage] == 1 then
			pauseScreen.currentPage = pauseScreen.pageNames.achiev1
			pauseScreen.currentHiPage = 2
			pauseScreen.currentOption[pauseScreen.currentPage]=1
		elseif pauseScreen.currentOption[pauseScreen.currentPage] == 2 then
			pauseScreen.currentPage = pauseScreen.pageNames.main
		end
	elseif pauseScreen.currentPage == pauseScreen.pageNames.achiev1 then
		if pauseScreen.currentOption[pauseScreen.currentPage] == 1 then
			pauseScreen.currentPage = pauseScreen.pageNames.achiev2
			pauseScreen.currentHiPage = 3
			pauseScreen.currentOption[pauseScreen.currentPage]=1
		elseif pauseScreen.currentOption[pauseScreen.currentPage] == 2 then
			pauseScreen.currentPage = pauseScreen.pageNames.main
		end	
	elseif pauseScreen.currentPage == pauseScreen.pageNames.achiev2 then
		if pauseScreen.currentOption[pauseScreen.currentPage] == 1 then
			pauseScreen.currentPage = pauseScreen.pageNames.highscores
			pauseScreen.currentHiPage = 1
			pauseScreen.currentOption[pauseScreen.currentPage]=1
		elseif pauseScreen.currentOption[pauseScreen.currentPage] == 2 then
			pauseScreen.currentPage = pauseScreen.pageNames.main
		end
	elseif pauseScreen.currentPage == pauseScreen.pageNames.delete then
		if pauseScreen.currentOption[pauseScreen.currentPage] == 1 then
			wipeHighscores()
			wipeAchievements()
			storeData()
		end
		pauseScreen.currentPage = pauseScreen.pageNames.main
	elseif pauseScreen.currentPage == pauseScreen.pageNames.credits then
		pauseScreen.currentPage = pauseScreen.pageNames.main
	elseif pauseScreen.currentPage == pauseScreen.pageNames.restart then
		if pauseScreen.currentOption[pauseScreen.currentPage] == 1 then
			countProgression()
			startMenu()
		end
		pauseScreen.currentPage = pauseScreen.pageNames.main
	elseif pauseScreen.currentPage == pauseScreen.pageNames.exit then
		if pauseScreen.currentOption[pauseScreen.currentPage] == 1 then
			exit()
		end
		pauseScreen.currentPage = pauseScreen.pageNames.main
	end
end

function exit()
	love.event.push("quit")
end

function contains(tab, n)
	for i,v in ipairs(tab) do
		if n == v then return true end
	end
	return false
end

function updateOptions(dt)
	pauseScreen.timer = pauseScreen.timer - dt
	if pauseScreen.timer <= 0 then
		pauseScreen.timer = pauseScreen.delay
	end

	-- joystick
	if joystickOpen then
		if loveVersion == 9 then
			dx = joystickOpen:getAxis(1)
			dy = joystickOpen:getAxis(2)
		elseif loveVersion == 8 then
           dx = love.joystick.getAxis(joystickOpen,1)
           dy = love.joystick.getAxis(joystickOpen,2)
	    end
	    if dx < -0.5 or dy < -0.5 then 
	    	if not pauseScreen.wasJoystickPressed then
	    		pauseScreen.wasJoystickPressed = true
	    		optionsUpPressed()
	    	end
	    elseif dx > 0.5 or dy > 0.5 then
	    	if not pauseScreen.wasJoystickPressed then
	    		pauseScreen.wasJoystickPressed = true
	    		optionsDownPressed()
	    	end
	    else
	    	pauseScreen.wasJoystickPressed = false
	    end
	end

end

function toggleFullscreen()
	isFullscreen = not isFullscreen
	if loveVersion == 8 then
		love.graphics.toggleFullscreen()
	else
		love.window.setFullscreen(isFullscreen)
	end
	love.mouse.setVisible(not isFullscreen)
	optionsReadFullscreen()
	storeData()
end

function toggleSound()
	pauseScreen.soundIsOn = not pauseScreen.soundIsOn
	startStopMusic()
	if pauseScreen.soundIsOn then
		pauseScreen.optionTexts[pauseScreen.pageNames.main][3] = "SOUND: ON"
	else
		pauseScreen.optionTexts[pauseScreen.pageNames.main][3] = "SOUND: OFF"
	end
	storeData()
end

function optionsReadFullscreen()
	if isFullscreen then
		pauseScreen.optionTexts[pauseScreen.pageNames.main][2] = "DISPLAY: FULLSCREEN"
	else
		pauseScreen.optionTexts[pauseScreen.pageNames.main][2] = "DISPLAY: WINDOWED"
	end
end

function selectNextGamepad()
	if loveVersion == 9 then
		local jc = love.joystick.getJoystickCount()
		local joys = love.joystick.getJoysticks()
		if not joystickOpen then
			-- try from 0 to 4
			joystickOpen = false
			for i,v in ipairs(joys) do
				if v:getButtonCount()>0 then
					joystickOpen = v
					break
				end
			end
		else
			local found = false
			local jo = joystickOpen
			joystickOpen = false
			for i,v in ipairs(joys) do
				if v == joystickOpen then
					found = true
				elseif found and v:getButtonCount()>9 then
					joystickOpen = v
					break
				end
			end
		end
	elseif loveVersion == 8 then
	 	if not joystickOpen then
	 		-- try from 0 to 4
	 		joystickOpen = false
	 		for i=0,4 do
				if love.joystick.open(i) and love.joystick.getNumButtons(i)>=0 then
	 				joystickOpen = i
	 				break
	 			end
	 		end
		else
			local jo = joystickOpen
			joystickOpen = false
			for i=jo+1,4 do
				if love.joystick.open(i) and love.joystick.getNumButtons(i)>=0 then
					joystickOpen = i
					break
				end
			end
		end
	end

	reloadControlNames()
end

function love.joystickadded()
	reloadControlNames()
	if not joystickOpen then loadJoystick() end
end

function reloadControlNames()
	if loveVersion == 9 then
		if joystickOpen then
			pauseScreen.optionTexts[1][1] = "GAMEPAD: "..string.upper(joystickOpen:getName())
		else
			pauseScreen.optionTexts[1][1] = "GAMEPAD: NONE"
		end
	elseif loveVersion == 8 then
		if joystickOpen then
			pauseScreen.optionTexts[1][1] = "GAMEPAD: "..string.upper(love.joystick.getName(joystickOpen))
		else
			pauseScreen.optionTexts[1][1] = "GAMEPAD: NONE"
		end
	end
end

function storeData()
	local str = ""
	str = str..serializeHighscores()
	str = str..serializeAchievements()

	str = str.."isFullscreen = "..tostring(isFullscreen).."\n"
	str = str.."pauseScreen.soundIsOn = "..tostring(pauseScreen.soundIsOn).."\n"

	love.filesystem.write("data.bin",recode(str),string.len(str))
end

function retrieveData()
	local datafile = love.filesystem.exists("data.bin")
	if datafile then 
		dataFile = love.filesystem.read("data.bin")
	end

	if dataFile then
		local wasFullscreen = isFullscreen
		local wasSoundOn = pauseScreen.soundIsOn
		loadstring(recode(dataFile))()
		parseAchievements()
		if isFullscreen ~= wasFullscreen then
			isFullscreen = not isFullscreen
			toggleFullscreen()
		end
		if pauseScreen.soundIsOn ~= wasSoundOn then
			pauseScreen.soundIsOn = not pauseScreen.soundIsOn
			toggleSound()
		end
	else
		-- if there is no data, enable fullscreen by default
		toggleFullscreen()
		return 
	end
end

function recode(str)
	xorarray = {215,100,200,204,233,50,85,196,71,141,122,160,93,131,243,234,162,183,36,155,4,62,35,205,40,102,33,27,255,55,131,214,156,75,163,134,126,249,74,197,134,197,102,228,72,90,206,235,17,243,134,22,49,169,227,89,16,5,117,16,60,248,230,217,68,138,96,194,131,170,136,10,112,238,238,184,72,189,163,90,176,42,112,225,212,84,58,228,89,175,244,150,168,219,112,236,101,208,175,233,123,55,243,235,37,225,164,110,158,71,201,78,114,57,48,70,142,106,43,232,26,32,126,194,252,239,175,98,191}
	function myxor (a,b)
	  local r = 0
	  for i = 0, 31 do
		local x = a / 2 + b / 2
		if x ~= math.floor (x) then
		  r = r + 2^i
		end
		a = math.floor (a / 2)
		b = math.floor (b / 2)
	  end
	  return r
	end
	local ndx = 1
	local ret = ""
	for i=1,string.len(str) do
		ret = ret..string.char(myxor(string.byte(str,i), xorarray[ndx]))
		ndx = (ndx%table.getn(xorarray)) + 1
	end
	return ret
end
