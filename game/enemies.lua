Enemies = {}

-- Enemies:
-- 1 normal. in formations
-- 2 snake. moves in a sinusoid
-- 3 mega. shoots in the direction of the player, every 4 shoots, harp of lasers
-- 4 mill. shoots a spiral of bullets
-- 5 bishop. moves diagonally, shots triple
-- 6 kamikaze.  moves in the direction of the player, then explodes
-- 7 hawk.  comes in a diagonal, leaves a trail of mines (todo)
-- 8 spiral. converges towards player in a spiral
-- 9 plant.  shoots a row of bullets upwards, they fall downwards
-- 10 clock.  blocks of enemies, they turn in 90-degrees arcs, keeping formation
-- 11 falcon.  moves in parabole, then shoots mg-style lasers
-- 12 swarm. come in formation, then break formation randomly, cat-like

-- todo: make more formations (also mixed formations?)

function startEnemies()
	Enemies.list = {}

	Enemies.spawner = {
		timer = 0,
		delay = 0
	}
	Enemies.deadCount = 4
end

function initEnemies()
	Enemies.prepareFrames()
	Enemies.enemyMultipliers = params.enemyMultipliers
end

function Enemies.wipe()
	Enemies.list = {}
end

function Enemies.getStartPoint()
	local sp = -1
	local var = 0.5
	local hw = bgImg:getWidth()*0.75
	while sp<0 or sp>hw*2 do
		sp = randn()*var*hw + hw
	end
	return sp
end

function Enemies.spawn()
	-- local startPos = { x = Enemies.getStartPoint(), y = -50}
	-- EnemyFactory.addNormal(startPos)


	-- Enemies.spawnFormation(difficulty.currentEnemy)
	Enemies.spawnFormation(Formations.getEnemyIndex())
end

function Enemies.isBigEnemy(et)
	local small = {1,2,5,6,8,10,12}
	for i,v in ipairs(small) do if v == et then return false end end
	return true
end

function Enemies.spawnFormation(et)
	local groupSize = Enemies.enemyMultipliers[et] * params.individualCount()
	local groupArray = Formations.getPattern(groupSize)
	local cols,rows = table.getn(groupArray[1]),table.getn(groupArray)
	local x0 = Enemies.getStartPoint()
	local separation = 16 + 16 * math.random(4)
	if Enemies.isBigEnemy(et) then
		separation = separation + 16
		if separation < 64 then separation = 64 end
	end

	local extradata = {}

	if et == 2 then
		-- special: snake
		extradata = EnemyFactory.prepareSnake(cols, rows)
	end

	if et == 10 then
		-- special: clock formation
		extradata = EnemyFactory.prepareClockFormation({x = x0 - cols/2*separation, y = -50},cols,rows)
	end

	for j=1,rows do
		for i = 1,cols do
			if groupArray[j][i] ~= 0 then
				if extradata then
					extradata.pos = {i-1,j-1}
				end
				EnemyFactory.spawnEnemy(et, {x = x0 + (i-cols/2) * separation, y = -50 - (j-1)*separation},extradata)
			end
		end
	end
end

function Enemies.prepareFrames()
	local img = love.graphics.newImage("img/playersheet4.png")
	Enemies.sprites = {}
	Enemies.batch = love.graphics.newSpriteBatch(img,2000,"static")

	for k=0,1 do
		for j=0,3 do
			for i=0,2 do
				table.insert(Enemies.sprites,
					love.graphics.newQuad((i+k*3)*64, j*64, 64, 64, img:getWidth(), img:getHeight()))
			end
		end
	end
end

function Enemies.checkDead()
	for j,b in ipairs(Bullets.playerList) do
		for i,v in ipairs(Enemies.list) do
			if not v.dead and checkBoxes(b,v) then
				Enemies.scheduleKill(i,false)
				registerEnemyKill(v.eType, b.bulletType)
				Bullets.scheduleKill(Bullets.playerList,j)
			end
		end
	end
end

function Enemies.purge()
	local n = table.getn(Enemies.list)
	for i=1,n do
		local v = Enemies.list[n-i+1]
		if not v.alive then
			if not v.silent then
				Explo.add(v.pos)
				playSound(Sound.enemyExplosion, v.pos.x-fgOvr.x, 0.5)
				if not v.noCount then
					Enemies.deadCount = Enemies.deadCount - 1
					if Enemies.deadCount <= 0 then
						Enemies.deadCount = params.newPickupSpawnCount()
						Pickups.addSingle(v.pos)
					end
				end
			end

			table.remove(Enemies.list,n-i+1)
		end
	end
end

function Enemies.scheduleKill(num, silent)
	Enemies.list[num].alive = false
	if silent then
		Enemies.list[num].silent = true 
	end
end

function Enemies.scheduleExplode(enemy)
	enemy.alive = false
	enemy.noCount = true
end

function Enemies.update(dt)
	-- kill
	Enemies.checkDead()

	-- spawn
	if not gameFinished then
		Enemies.spawner.timer = Enemies.spawner.timer - dt
		if Enemies.spawner.timer <= 0 then
			Enemies.spawn()
			Enemies.spawner.timer = Enemies.spawner.timer + Enemies.spawner.delay
			Enemies.spawner.delay = params.newSpawnerDelay()
		end
	end

	-- update
	for i,v in ipairs(Enemies.list) do
		-- snake
		if v.eType == EnemyTypes.snake then
			v.centerX = v.centerX + v.dir.x * v.speed * dt
			v.oscPhase = v.oscPhase + dt * v.oscSpeed
			v.pos.x = v.centerX + math.cos(v.oscPhase) * v.oscAmp
		end

		-- kamikaze
		if v.eType == EnemyTypes.kamikaze then
			if Player.p.alive then
				local dir = {x = Player.p.pos.x + fgOvr.x - v.pos.x, y = Player.p.pos.y - v.pos.y }
				local ang = math.atan2(v.dir.y,v.dir.x)
				local newang = math.atan2(dir.y,dir.x)
				local angdiff = newang - ang
				
				if angdiff < -math.pi then angdiff = math.pi*2 + angdiff end
				if angdiff > math.pi then angdiff = - math.pi*2 + angdiff end

				if angdiff < -v.maxSteer then angdiff = -v.maxSteer end
				if angdiff > v.maxSteer then angdiff = v.maxSteer end
				v.dir = {x = math.cos(ang + angdiff), y = math.sin(ang + angdiff)}
			end
			v.angle = math.atan2(v.dir.y, v.dir.x) - math.pi/2
		end

		-- spiral
		if v.eType == EnemyTypes.spiral then
			if Player.p.alive and not Player.p.spawning then
				local dir = normalize({x = Player.p.pos.x + fgOvr.x - v.pos.x, y = Player.p.pos.y - v.pos.y })
				local tg1 = {x = -dir.y, y = dir.x}
				local tg2 = {x = dir.y, y = -dir.x}
				-- select tangent
				local c1 = v.dir.x * tg1.x + v.dir.y * tg1.y
				local c2 = v.dir.x * tg2.x + v.dir.y * tg2.y
				local tg = tg1
				if c2 > c1 then tg = tg2 end

				-- compose
				v.dir = normalize{x = (1-v.attraction) * tg.x + v.attraction * dir.x, y = (1-v.attraction) * tg.y + v.attraction * dir.y}
				v.angle = math.atan2(v.dir.y,v.dir.x) - math.pi/2
			end
			v.angle = math.atan2(v.dir.y,v.dir.x) - math.pi/2
		end

		-- move
		v.pos.x = v.pos.x + v.dir.x * v.speed * dt
		v.pos.y = v.pos.y + v.dir.y * v.speed * dt

		-- chicken
		if v.eType == EnemyTypes.chicken then
			-- chicken
			v.speed = v.speed * math.pow(v.slowdown,dt*60)
			if v.speed < v.disapThres then
				Enemies.scheduleExplode(v)
			end
			v.angle = v.angle + v.drift * dt
		end

		-- clock formation
		if v.eType == EnemyTypes.clock then
			
			v.advanceTime = v.advanceTime - dt
			if v.advanceTime <= 0 then
				if v.turnState == 0 then
					v.turnAngle = math.atan2(v.dir.y,v.dir.x)
					v.advanceTime = v.separation / v.speed
					v.turnState = 1
					v.newTurnAngle = v.turnAngle + math.pi/2
					v.turnFreq = math.pi/2/v.advanceTime
					v.turnCenter = { 
							x = v.pos.x + v.separation * math.cos(v.turnAngle + math.pi/2), 
							y = v.pos.y + v.separation * math.sin(v.turnAngle + math.pi/2)
						}
					v.fixedPoint = {
							x = v.pos.x + v.separation * (math.cos(v.turnAngle) + math.cos(v.turnAngle + math.pi/2)), 
							y = v.pos.y + v.separation * (math.sin(v.turnAngle) + math.sin(v.turnAngle + math.pi/2)) + 
								v.scrollSpeed * v.advanceTime
						}
				else
					v.turnState = 0
					v.dir = { x = math.cos(v.newTurnAngle), y = math.sin(v.newTurnAngle)}

					v.dirState = v.dirState + 1
					while table.getn(v.formationControl.steps) < v.dirState do
						table.insert(v.formationControl.steps, math.random(4))
					end
					v.advanceTime = v.separation * (v.formationControl.steps[v.dirState]+(v.seq*2)) / v.speed
					v.seq = v.formationControl.seqLen - 1 - v.seq
					v.pos = { x = v.fixedPoint.x, y = v.fixedPoint.y }
				end
			end

			if v.turnState == 1 then
				v.dir = { x = math.cos(v.turnAngle), y = math.sin(v.turnAngle)}
				v.turnCenter.y = v.turnCenter.y + v.scrollSpeed * dt
				v.pos = { 
					x = v.turnCenter.x + math.cos(v.turnAngle - math.pi/2) * v.separation, 
					y = v.turnCenter.y + math.sin(v.turnAngle - math.pi/2) * v.separation }
				v.turnAngle = v.turnAngle + v.turnFreq * dt
			else
				-- vertical gravity
				v.pos.y = v.pos.y + v.scrollSpeed * dt
			end
		end

		-- swarm
		if v.eType == EnemyTypes.swarm then
			if v.switchSign == 1 then
				v.currentFactor = decreaseExponential(dt, v.currentFactor, v.switchFactor)
			elseif v.switchSign == -1 then
				v.currentFactor = increaseExponential(dt, v.currentFactor, v.switchFactor)
			end
			v.speed = v.origSpeed * v.currentFactor + v.newSpeed * (1-v.currentFactor)
			v.dir.x = v.origDir.x * v.currentFactor + v.newDir.x * (1-v.currentFactor)
			v.dir.y = v.origDir.y * v.currentFactor + v.newDir.y * (1-v.currentFactor)

			v.switchTimer = v.switchTimer - dt
			if v.switchTimer <= 0 then
				v.switchSign = -v.switchSign
				if v.switchSign == 1 then
					v.newSpeed = v.baseSpeed + math.random()*v.varSpeed
					local ang = math.random() * 2 * math.pi
					v.newDir = { x = math.cos(ang), y = math.sin(ang) }
					v.switchDelay = v.switchDelayUp
				else
					v.switchDelay = v.switchDelayDown
				end
				v.switchTimer = v.switchDelay
			end
		end

		-- falcon
		if v.eType == EnemyTypes.falcon then
			v.advanceTime = v.advanceTime - dt
			if v.advanceTime <= 0 then
				if v.shootState == 0 then
					v.advanceTime = v.rotDelay
					v.shootState = 1
					v.turnAngle = 0
					v.turnFreq = math.pi/2/v.advanceTime
					v.moveSign = signOf(math.random()-0.5)
					v.turnCenter = { 
							x = v.pos.x - v.moveSign * v.separation, 
							y = v.pos.y
						}
					v.speed = v.scrollSpeed
				elseif v.shootState == 1 then
					v.advanceTime = v.shootingDelay
					v.shootState = 2
					v.speed = v.shootSpeed
					v.dir = { x = 0, y = -1 }
					v.shootDelay = v.mgDelay
					v.shootTimer = 0
				elseif v.shootState == 2 then
					v.advanceTime = v.downDelay
					v.shootState = 0
					v.speed = v.moveSpeed
					v.dir = { x = 0, y = 1 }
					v.shootDelay = v.normalBulletDelay
					v.shootTimer = 0
				end
			end

			-- rotate
			if v.shootState == 1 then
				v.turnAngle = v.turnAngle + v.turnFreq * dt
				v.turnCenter.y = v.turnCenter.y + v.scrollSpeed * dt
				v.pos.x = v.turnCenter.x + v.moveSign * v.separation * math.cos(v.turnAngle)
				v.pos.y = v.turnCenter.y + v.separation * math.sin(v.turnAngle)
			elseif v.shootState == 2 then
				v.speed = decreaseExponential(dt, v.speed, v.knockbackRate)
			end
		end

		-- out of screen
		if v.eType == EnemyTypes.hawk and outOfScreenSide(v.pos, v.w/2, v.h/2) then
			Enemies.scheduleKill(i, true)
		elseif v.eType == EnemyTypes.clock then
			if belowOneHalfScreen(v.pos, v.h/2) then
				Enemies.scheduleKill(i, true)
			end
		elseif belowScreen(v.pos, v.h/2) then
			Enemies.scheduleKill(i, true)
		end

		-- rotate
		if v.rotating then
			v.angle = v.angle + dt * math.pi*2 / v.rotTime * v.rotSign
			if v.eType == EnemyTypes.mill and v.shootState == v.rotState then
				if math.abs(v.angle) >= math.pi*2 then
					v.shootState = 0
					v.dir = {x = 0, y = 1}
					v.angle = 0
					v.rotating = false
					v.shootDelay = v.shootFirstDelay
					v.shootTimer = v.shootDelay
				end
			end
		end


		-- shoot
		if not v.cannotShoot then
			v.shootTimer = v.shootTimer - dt
			if v.shootTimer <= 0 then
				Enemies.shoot(v)
			end
		end
	end

	Enemies.purge()
end

function Enemies.shoot(enemy)
	enemy.shootTimer = enemy.shootTimer + enemy.shootDelay

	if enemy.eType == EnemyTypes.mega then
		-- mega
		enemy.shootCount = enemy.shootCount - 1
		if enemy.shootCount <= 0 then
			-- shoot lasers
			enemy.shootCount = enemy.shootLoop
			if enemy.laserEnabled then
				local la = math.floor(enemy.laserAmount/2)
				for i=-la, la do
					Bullets.addLaserBullet(enemy.pos, math.pi/enemy.laserAmount*i)
				end
			end
		else
			if enemy.homingEnabled then
				Bullets.addHomingBullet(enemy.pos)
			end
		end
	elseif enemy.eType == EnemyTypes.mill then
		-- mill
		if enemy.shootState < enemy.rotState then
			enemy.shootState = enemy.shootState + 1

			if enemy.shootState == enemy.rotState then
				-- start spinning
				enemy.shootDelay = enemy.shootSecondDelay
				enemy.shootTimer = 0
				enemy.dir = {x = 0, y = 0}
				enemy.rotSign = 1
				enemy.rotating = true
				if math.random()<0.5 then enemy.rotSign = -1 end
			else
				local sc = false
				if enemy.shootState == 2 then sc = true end
				if enemy.laserEnabled then
					Bullets.addEnemyFourLasers(enemy.pos, sc)
				end
			end
		elseif enemy.shootState == enemy.rotState then
			-- continue spinning
			Bullets.addEnemyDirBullet(enemy.pos, enemy.angle)
		end
	elseif enemy.eType == EnemyTypes.bishop then
		-- bishop
		local ang = math.pi*50/180
		if enemy.angle < 0 then
			ang = -math.pi*50/180
		end
		local la = math.floor(enemy.bulletCount/2)
		for i=-la,la do
			Bullets.addEnemyFasterDirBullet(enemy.pos, ang + i*enemy.bulletSeparation*math.pi/180)
		end
	elseif enemy.eType == EnemyTypes.kamikaze then
		-- kamikaze
		Enemies.scheduleExplode(enemy)
		EnemyFactory.addChickenGroup(enemy.pos)
	elseif enemy.eType == EnemyTypes.hawk then
		-- hawk
		Bullets.addRasberryBullet(enemy.pos, enemy.dir)
	elseif enemy.eType == EnemyTypes.spiral then
		-- spiral
		Bullets.addEnemyFasterDirBullet(enemy.pos, enemy.angle)
	elseif enemy.eType == EnemyTypes.plant then
		-- plant
		Bullets.addEnemyPlantBullets(enemy.pos)
	elseif enemy.eType == EnemyTypes.falcon then
		-- falcon
		if enemy.shootState == 2 then
			Bullets.addMgLaserBullet(enemy.pos)
			-- knockback
			enemy.speed = enemy.knockbackSpeed
		else
			Bullets.addEnemyBullet(enemy.pos)
		end
	else
		-- default
		Bullets.addEnemyBullet(enemy.pos)
	end
end

function Enemies.draw()
	Enemies.batch:clear()
	for i,v in ipairs(Enemies.list) do
		local sc = v.scale or 1
		-- Enemies.batch:add(Enemies.sprites[v.spriteNdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, sc, sc, 32, 32)
		addToBatch(Enemies.batch,Enemies.sprites[v.spriteNdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), v.angle, sc, sc, 32, 32)
	end
	love.graphics.draw(Enemies.batch)
end
