


function initScoreDisplay()
	ScoreDisplay = {
		page = 1,
		pageCount = 1 + achievementPageCount(),
		delay = 5,
		timer = 5
	}
end

function startScoreDisplay()
	ScoreDisplay.page = 1
	ScoreDisplay.timer = ScoreDisplay.delay
end

function updateScoreDisplay(dt)
	ScoreDisplay.timer = ScoreDisplay.timer - dt
	if ScoreDisplay.timer <= 0 then
		ScoreDisplay.timer = ScoreDisplay.delay
		ScoreDisplay.page = (ScoreDisplay.page % ScoreDisplay.pageCount) + 1
	end
end


function drawScores()
	if ScoreDisplay.page == 1 then
		drawHighscores()
		drawProgress()
	else
		printAchievementsTable(ScoreDisplay.page - 1)
	end
end

function saveScore()
	table.insert(highscoreTable, {
		string.upper(os.date()),
		Progression.waveCount,
		string.format("%.2f%s",Progression.percentage,"%"),
		Progression.percentage
	})

	table.sort(highscoreTable, function(a,b) return a[4]>b[4] end)

	while table.getn(highscoreTable)>5 do
		table.remove(highscoreTable,6)
	end

	storeData()
end

function serializeHighscores()
	local str = ""
	if table.getn(highscoreTable) > 0 then
		str = "highscoreTable = {"
		for i,v in ipairs(highscoreTable) do
			str = str.."{ \""..v[1].."\","
			str = str..v[2]..","
			str = str.."\""..v[3].."\","
			str = str..v[4].."}"
			if i < table.getn(highscoreTable) then
				str = str..",\n"
			else
				str = str.."}\n"
			end
		end
	end
	return str
end

function wipeHighscores()
	highscoreTable = {}
end

function drawHighscores(opacity)
	opacity = opacity or 1
	if table.getn(highscoreTable) <= 0 then return end
	local y0 = screenSize.y/2-40
	printShadowedText("RANK",200,y0,opacity)
	printShadowedText("DATE",250,y0,opacity)
	printShadowedText("SECTOR",480,y0,opacity)
	printShadowedText("PROGRESS",550,y0,opacity)
	for i,v in ipairs(highscoreTable) do
		local yy = y0+20*i
		printShadowedText(i,200,yy,opacity)
		printShadowedText(v[1],250,yy,opacity)
		printShadowedText(v[2],480,yy,opacity)
		printShadowedText(v[3],550,yy,opacity)
	end
end
