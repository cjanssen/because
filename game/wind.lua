
function initWind()
end

function startWind()
	wind = {
		active = false,
		dir = { x = 0, y = 0 },
		phase = 0,
		freq = 0.8,
		amp = 0,
		max = 250
	}
end

function activateWind()
	wind.active = true
	wind.phase = math.pi
end

function stopWind()
	wind.active = false
end

function updateWind(dt)
	if wind.active then
		if Pickups.current ~= PickupTypes.wind then
			stopWind()
		end
		wind.phase = wind.phase + dt * wind.freq
		if wind.phase >= math.pi then
			wind.phase = wind.phase - math.pi
			local angle = math.random() * math.pi * 2
			wind.dir = { x = math.cos(angle), y = math.sin(angle) }
		end
		local amount = math.sin(wind.phase)
		wind.amp = wind.max * amount * amount
	else
		if Pickups.current == PickupTypes.wind then
			activateWind()
		end
	end
end
