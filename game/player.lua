Player = {}

function startPlayer()
	Player.p = {
		index = 1,
		pos = {x = screenSize.x/2, y = 400 },
		speed = 2500,
		drag = 0.55,
		dir = {x = 0, y = 0},
		acc = 2.5,
		vel = {x = 0, y = 0},
		w = 8,
		h = 8,
		shootTimer = 0,
		shootDelay = 0.1,
		alive = true,
		baseFrame = 0,
		nextBaseFrame = 0,
		switchFrameTime = 0,
		switchFrameDelay = 0.8,

		rotating = false,
		angle = 0,
		rotDir = 1,
		rotSpeed = 2,
		rotAccel = 20,

		immortal = false,
		immortalTime = 0,
		immortalDelay = 12,

		spawning = false,
		spawnTime = 2,
		spawnDelay = 2,
		spawnControlFraction = 0.75,
		spawnTime = 0,
		spawnBlinkDelay = 0.2,
		spawnSpeed = 1500,
		liveSpeed = 2500,

		maxLives = 3,
		lives = 3,

		controlsDisabled = false
	}
	Player.p.frameIdx = 3
	Player.p.frameTimer = 0
	Player.p.frameDelay = 0.1

	borders = {
		top = 24,
		bottom = 28
	}
end

function Player.spawn()
	Player.p.spawning = true
	Player.p.spawnTime = Player.p.spawnDelay
	Player.p.alive = true
	Player.p.pos.y = screenSize.y
	Player.p.dir.x = (screenSize.x/2 - Player.p.pos.x)/170
	Player.p.dir.y = -1
	Player.p.speed = Player.p.spawnSpeed
	Player.p.spawnBlinkTime = Player.p.spawnBlinkDelay
	Pickups.setCurrent(PickupTypes.normal)
	Player.p.controlsDisabled = false
end

function initPlayer()
	Player.prepareFrames()
end

function Player.prepareFrames()
	local img = love.graphics.newImage("img/playersheet4.png")
	Player.frames = {}
	Player.xscale = {}
	Player.batch = love.graphics.newSpriteBatch(img, 4, "static")
	local lastFrame = 1
	for k = 0,1 do
		for j = 0,2 do
			-- left leaning frames
			for i=0,1 do
				table.insert(Player.frames,
					love.graphics.newQuad((2-i+k*3)*64, 64 * (7-j), 64, 64, img:getWidth(), img:getHeight()))
				table.insert(Player.xscale, -1)
			end

			-- right leaning frames
			for i=0,2 do
				table.insert(Player.frames,
					love.graphics.newQuad((i+k*3)*64, 64 * (7-j), 64, 64, img:getWidth(), img:getHeight()))
				table.insert(Player.xscale, 1)

			end
		end
	end
end

function Player.update(dt)
	if not Player.p.alive then return end

	-- superinertia pickup
	if Pickups.current == PickupTypes.superspeed then
		Player.p.drag = 0.85
	else
		Player.p.drag = 0.55
	end

	Player.evalKeys()
	-- anims
	Player.updateAnims(dt)

	Player.move(dt)
	Player.updateShotTimer(dt)
	Player.checkBullets()
	Player.checkEnemyCollision()
	Player.checkPickups()
	Player.checkLifeCollected()
	Player.updateImmortality(dt)
	Player.updateFrames(dt)
	Player.updateSpawn(dt)

	-- Player.p.immortal = true
end

function Player.updateSpawn(dt)
	if Player.p.spawning then
		Player.p.spawnTime = Player.p.spawnTime - dt
		if Player.p.spawnTime <= Player.p.spawnDelay * Player.p.spawnControlFraction then
			Player.p.speed = Player.p.liveSpeed
			Player.p.dir = { x = 0, y = 0 }
		end
		if Player.p.spawnTime <= 0 then
			Player.p.spawning = false
		end

		Player.p.spawnBlinkTime = Player.p.spawnBlinkTime - dt
		while Player.p.spawnBlinkTime <= 0 do
			Player.p.spawnBlinkTime = Player.p.spawnBlinkTime + Player.p.spawnBlinkDelay
		end
	end
end

function Player.updateFrames(dt)
	local p = Player.p
	if p.nextBaseFrame ~= p.baseFrame then
		if p.switchFrameTime <= 0 then
			p.switchFrameTime = p.switchFrameDelay
		else
			p.switchFrameTime = p.switchFrameTime - dt
			if p.switchFrameTime <= 0 then
				p.baseFrame = p.nextBaseFrame
			end
		end
	end
end

function Player.activateImmortality()
	-- activate
	Player.p.immortal = true
	Player.p.immortalTime = Player.p.immortalDelay
	Player.p.nextBaseFrame = 3
end

function Player.updateImmortality(dt)
	local p = Player.p
	-- special: immortality pickup

	if p.immortalTime > 0 or p.immortal then
		if Pickups.current ~= PickupTypes.invulnerability and p.immortalTime > p.switchFrameDelay then
			p.immortalTime = p.switchFrameDelay
		end

		p.immortalTime = p.immortalTime - dt
		if p.immortalTime <= p.switchFrameDelay then
			p.nextBaseFrame = 0
		end
		if p.immortalTime <= 0 then
			if Pickups.current == PickupTypes.invulnerability then
				Pickups.setCurrent(PickupTypes.normal)
			end
			p.immortal = false
		end
	end
end

function Player.updateAnims(dt)
	Player.p.frameTimer = Player.p.frameTimer - dt
	if Player.p.frameTimer <= 0 then
		Player.p.frameTimer = Player.p.frameDelay
		if Player.p.dir.x < 0 or (Player.p.dir.x == 0 and Player.p.frameIdx>3) then
			Player.p.frameIdx = Player.p.frameIdx - 1
			if Player.p.frameIdx < 1 then Player.p.frameIdx = 1 end
		end
		if Player.p.dir.x > 0 or (Player.p.dir.x == 0 and Player.p.frameIdx<3) then
			Player.p.frameIdx = Player.p.frameIdx + 1
			if Player.p.frameIdx > 5 then Player.p.frameIdx = 5 end
		end

	end
end

function Player.updateShotTimer(dt)
	if Player.p.shootTimer > 0 then
		Player.p.shootTimer = Player.p.shootTimer - dt
	end
	Player.adjustShootTimer()
end

function Player.adjustShootTimer()
	if Pickups.current == PickupTypes.sinus then
		Player.p.shootDelay = 0.16
	elseif Pickups.current == PickupTypes.cheese then
		Player.p.shootDelay = 0.4
	elseif Pickups.current == PickupTypes.card then
		Player.p.shootDelay = 0.124
	elseif Pickups.current == PickupTypes.cat then
		Player.p.shootDelay = 0.3
	elseif Pickups.current == PickupTypes.sushi then
		Player.p.shootDelay = 0.7
	elseif Pickups.current == PickupTypes.bottle then
		Player.p.shootDelay = 0.18
	elseif Pickups.current == PickupTypes.heart then
		Player.p.shootDelay = 0.35
	elseif Pickups.current == PickupTypes.flower then
		Player.p.shootDelay = 1.85
	else
		Player.p.shootDelay = 0.1
	end
	if Player.p.shootTimer > Player.p.shootDelay then
		Player.p.shootTimer = Player.p.shootDelay
	end
end

function Player.move(dt)
	local p = Player.p
	local drag = math.pow(p.drag,dt*60)
	
	-- rotation pickup
	if Pickups.current == PickupTypes.rotate then
		if not p.rotating then
			p.rotating = true
			p.angle = 0
			p.rotDir = 1
			if math.random() < 0.5 then
				p.rotDir = -1
			end
		end
		p.angle = ( p.angle + p.rotSpeed * p.rotDir * dt ) % (math.pi*2)
	else
		p.rotating = false
		if math.abs(p.angle) > 0.01 then
			if math.abs(p.angle) > math.pi then p.rotDir = 1 else p.rotDir = -1 end
			p.angle = ( p.angle + p.rotSpeed * p.rotDir * p.rotAccel * dt )
			if p.angle > 2*math.pi or p.angle < 0 then
				p.angle = 0
			end
		end
	end

	local cx = math.cos(p.angle)
	local sx = math.sin(p.angle)

	p.dir = { x = p.dir.x * cx - p.dir.y * sx, y =  p.dir.x * sx + p.dir.y * cx }

	p.vel = { x = p.vel.x * drag + p.dir.x * p.acc * dt,
			  y = p.vel.y * drag + p.dir.y * p.acc * dt }

	p.pos = { x = p.pos.x + p.vel.x * p.speed * dt,
			  y = p.pos.y + p.vel.y * p.speed * dt * 1.5 }

	-- wind pickup
	if wind.active then
		p.pos.x = p.pos.x + wind.dir.x * wind.amp * dt
		p.pos.y = p.pos.y + wind.dir.y * wind.amp * dt
	end



	if p.pos.x < 0 then p.pos.x = 0 end
	if p.pos.x > screenSize.x then p.pos.x = screenSize.x end
	if p.pos.y < borders.top and not gameFinished then 
		p.pos.y = borders.top
		if p.vel.y < 0 then p.vel.y = 0 end
	end
	if p.pos.y > screenSize.y - borders.bottom then 
		p.pos.y = screenSize.y - borders.bottom
		if p.vel.y > 0 then p.vel.y = 0 end
	end
end

function Player.draw()
	local p = Player.p
	if not p.alive then return end

	Player.batch:clear()
	Player.batch:setColor(mushroom.currentColor.r, mushroom.currentColor.g, mushroom.currentColor.b)

	if p.spawning then
		if p.spawnBlinkTime <= p.spawnBlinkDelay/2 then
			addToBatch(Player.batch,Player.frames[p.frameIdx+p.baseFrame*5], math.floor(p.pos.x), math.floor(p.pos.y), p.angle, Player.xscale[p.frameIdx+p.baseFrame*5], 1, 32, 32)
		else
			addToBatch(Player.batch,Player.frames[p.frameIdx+5*3], math.floor(p.pos.x), math.floor(p.pos.y), p.angle, Player.xscale[p.frameIdx+5*3], 1, 32, 32)
		end
	elseif p.switchFrameTime > 0 then
		local fraction = (p.switchFrameTime / p.switchFrameDelay)
		Player.batch:setColor(mushroom.currentColor.r, mushroom.currentColor.g, mushroom.currentColor.b,255*fraction)
		addToBatch(Player.batch,Player.frames[p.frameIdx+p.baseFrame*5], math.floor(p.pos.x), math.floor(p.pos.y), p.angle, Player.xscale[p.frameIdx+p.baseFrame*5], 1, 32, 32)
		
		love.graphics.draw(Player.batch)
		Player.batch:setColor(mushroom.currentColor.r, mushroom.currentColor.g, mushroom.currentColor.b)
		Player.batch:clear()

		Player.batch:setColor(mushroom.currentColor.r, mushroom.currentColor.g, mushroom.currentColor.b,255*(1-fraction))
		addToBatch(Player.batch,Player.frames[p.frameIdx+p.nextBaseFrame*5], math.floor(p.pos.x), math.floor(p.pos.y), p.angle, Player.xscale[p.frameIdx+p.nextBaseFrame*5], 1, 32, 32)

		-- reset color
		Player.batch:setColor(255,255,255)
	else
		addToBatch(Player.batch,Player.frames[p.frameIdx+p.baseFrame*5], math.floor(p.pos.x), math.floor(p.pos.y), p.angle, Player.xscale[p.frameIdx+p.baseFrame*5], 1, 32, 32)
	end
	love.graphics.draw(Player.batch)
end

function Player.shoot()
	if Player.p.shootTimer <= 0 then
		Player.p.shootTimer = Player.p.shootDelay
		if Pickups.current == PickupTypes.sinus then
			Bullets.addSinusBullet(Player.p.pos)
			playSound(Sound.sinusBullet, Player.p.pos.x)
		elseif Pickups.current == PickupTypes.cheese then
			Bullets.addCheeseBullet(Player.p.pos)
			-- recoil
			Player.p.vel.y = Player.p.vel.y + 0.25
			playSound(Sound.cheeseBullet, Player.p.pos.x)
		elseif Pickups.current == PickupTypes.card then
			Bullets.addCardBullet(Player.p.pos)
			playSound(Sound.cardBullet, Player.p.pos.x)
		elseif Pickups.current == PickupTypes.cat then
			Bullets.addCatBullet(Player.p.pos)
			playSound(Sound.catBullet, Player.p.pos.x)
		elseif Pickups.current == PickupTypes.sushi then
			Bullets.addSushiBullet(Player.p.pos)
			-- recoil
			Player.p.vel.y = Player.p.vel.y + 0.55
			playSound(Sound.sushiBullet, Player.p.pos.x)
		elseif Pickups.current == PickupTypes.bottle then
			Bullets.addBottleBullet(Player.p.pos)
			playSoundMono(Sound.bottleBullet, Player.p.pos.x, 0.3)
			-- playSound(Sound.bottleBullet, Player.p.pos.x, 0.3)
		elseif Pickups.current == PickupTypes.heart then
			Bullets.addHeartBullet(Player.p.pos)
			playSound(Sound.heartBullet, Player.p.pos.x)
		elseif Pickups.current == PickupTypes.flower then
			Bullets.addFlowerBullet(Player.p.pos)
			playSound(Sound.flowerBullet, Player.p.pos.x)
		elseif Pickups.current == PickupTypes.satellites then
			Bullets.addSimplePlayerBullet(Player.p.pos)
			playSound(Sound.smallBullet, Player.p.pos.x)
		else
			Bullets.addPlayerBullet(Player.p.pos)
			playSound(Sound.normalBullet, Player.p.pos.x)
		end
	end
end

function Player.kill()
	-- Player.p.immortal = true
	if not Player.p.immortal then
		Player.p.alive = false
		registerPlayerDied()
		Explo.add({x = Player.p.pos.x + fgOvr.x, y = Player.p.pos.y })
		Satellites.discard()
		Player.p.lives = Player.p.lives - 1

		-- reset display
		Player.p.switchFrameTime = 0
		Player.p.baseFrame = 0
		Player.p.nextBaseFrame = 0

		startLifeSpawnerTimer()
		countProgression()
		promptNewgame()
		playSound(Sound.playerDeath, Player.p.pos.x)
	end
end

function Player.checkBullets()
	local modif = {
		pos = { x = Player.p.pos.x + fgOvr.x, y = Player.p.pos.y },
		w = Player.p.w,
		h = Player.p.h
		}
	for i,v in ipairs(Bullets.enemyList) do
		if not v.dead and not Player.p.spawning and checkBoxes(v, modif) then
			Player.kill()
			Bullets.scheduleKill(Bullets.enemyList,i,Player)
		end
	end
end

function Player.checkEnemyCollision()
	local modif = {
		pos = { x = Player.p.pos.x + fgOvr.x, y = Player.p.pos.y },
		w = Player.p.w,
		h = Player.p.h
		}
	for i,v in ipairs(Enemies.list) do
		if v.alive and not Player.p.spawning and checkBoxes(modif,v) then
			Player.kill()
			Enemies.scheduleExplode(v)
			-- registerEnemyKill(v.eType, BulletTypes.playercolli)
		end
	end
end

function Player.checkPickups()
	local modif = {
		pos = { x = Player.p.pos.x + fgOvr.x, y = Player.p.pos.y },
		w = Player.p.w,
		h = Player.p.h
		}
	for i,v in ipairs(Pickups.list) do
		if checkBoxes(modif,v) then
			Pickups.activate(v)
			Player.p.shootTimer = 0
			Player.selectShip()
			v.valid = false
			if Pickups.current == PickupTypes.invulnerability then
				Player.activateImmortality()
			end
		end
	end
end

function Player.checkLifeCollected()
	local modif = {
		pos = { x = Player.p.pos.x + fgOvr.x, y = Player.p.pos.y },
		w = Player.p.w,
		h = Player.p.h
		}
	for i,v in ipairs(LifeSpawner.list) do
		if checkBoxes(modif,v) then
			collectLife(v)
		end
	end
end


function Player.selectShip()
	if Pickups.current == PickupTypes.wind then
		Player.p.nextBaseFrame = 1
	elseif Pickups.current == PickupTypes.superspeed then
		Player.p.nextBaseFrame = 2
	elseif Pickups.current == PickupTypes.invulnerability then
		Player.p.nextBaseFrame = 3
	else
		Player.p.nextBaseFrame = 0
	end
end

function Player.getInput(inputNumber)
	if inputNumber == 1 then
		-- up
		return love.keyboard.isDown("w") or love.keyboard.isDown("up")
		
	elseif inputNumber == 2 then
		-- down
		return love.keyboard.isDown("s") or love.keyboard.isDown("down")
	elseif inputNumber == 3 then
		-- left
		return love.keyboard.isDown("a") or love.keyboard.isDown("left")
	elseif inputNumber == 4 then
		-- right
		return love.keyboard.isDown("right") or love.keyboard.isDown("d")
	elseif inputNumber == 5 then
		-- fire
		return love.keyboard.isDown("lctrl") or love.keyboard.isDown("lshift")
			or love.keyboard.isDown("rctrl") or love.keyboard.isDown("rshift")
			or love.keyboard.isDown(" ")
	end
end

function Player.evalKeys()
	if Player.p.spawning and Player.p.spawnTime > Player.p.spawnControlFraction * Player.p.spawnDelay then
		return
	end

	if Player.p.controlsDisabled then
		return
	end

	local dir = Player.p.dir

	dir.x = 0
	if Player.getInput(3) then
		dir.x = -1
	end

	if Player.getInput(4) then
		dir.x = 1
	end

	dir.y = 0
	if Player.getInput(1) then
		dir.y = -1
	end

	if Player.getInput(2) then
		dir.y = 1
	end

	if joystickOpen then
		if loveVersion == 9 then
			dx = joystickOpen:getAxis(1)
			dy = joystickOpen:getAxis(2)
			if joystickOpen:isGamepad() then
				if joystickOpen:isGamepadDown("dpup") then dy = -1 end
				if joystickOpen:isGamepadDown("dpdown") then dy = 1 end
				if joystickOpen:isGamepadDown("dpleft") then dx = -1 end
				if joystickOpen:isGamepadDown("dpright") then dx = 1 end
			end
		elseif loveVersion == 8 then
           dx = love.joystick.getAxis(joystickOpen,1)
           dy = love.joystick.getAxis(joystickOpen,2)
	    end
	    if math.abs(dx)<0.2 then dx = 0 end
	    if math.abs(dy)<0.2 then dy = 0 end

		if dx ~= 0 or dy~= 0 then
			dir.x = dx
			dir.y = dy
		end
	end

	Player.p.dir = normalize(dir)

	local doShoot = false
	if Player.getInput(5) then
		doShoot = true
	end

	if joystickOpen then
		if loveVersion == 9 then
			if joystickOpen:isGamepad() then
				doShoot = joystickOpen:isGamepadDown("x","y","a","b","leftstick","rightstick","leftshoulder","rightshoulder")
			elseif joystickSpecial then
				local nb = joystickOpen:getButtonCount()
				for i=0,nb-1 do
					if joystickOpen:isDown(i) and (i~=9 and i~=10) then
						doShoot = true
						break
					end
				end
			else
				local nb = joystickOpen:getButtonCount()
				for i=0,nb-1 do
					if joystickOpen:isDown(i) and ((nb>14 and i~=3 and i~=4) or (nb<15 and (i < 4 or i > 10))) then
						doShoot = true
						break
					end
				end
			end
		elseif loveVersion == 8 then
			if joystickSpecial then
				local nb = love.joystick.getNumButtons(joystickOpen)
				for i=0,nb-1 do
					if love.joystick.isDown(joystickOpen,i) and (i~=9 and i~=10) then
						doShoot = true
						break
					end
				end
			else
				local nb = love.joystick.getNumButtons(joystickOpen)
				for i=1,nb do
					if love.joystick.isDown(joystickOpen,i) and ((nb>14 and i~=3 and i~=4) or (nb<15 and (i < 4 or i > 10))) then
						doShoot = true
						break
					end
				end
			end
		end
	end

	if doShoot then
		Player.adjustShootTimer()
		Player.shoot()
	end
end
