function initEndGame()
	PlanetFgImg = love.graphics.newImage("img/planet_fg.png")
	PlanetBgImg = love.graphics.newImage("img/planet_bg.png")
end

function prepareEndGame()
	gameFinished = false
	EndGameTimer = 15
	EndGameOpacity = 0
	PlanetPos = {x=900,y=-PlanetBgImg:getHeight()*2.7}
	PlanetSpeed = 110
end

function endGame()
	gameFinished = true
	gameState = 3
	Progression.percentage = 100
	if state ~= 0 then Pickups.current = PickupTypes.normal end
	EndGameMusicPlaying = false
	registerEndGame()
	saveScore()
end

function updateEndgame(dt)
	if not gameFinished then return end
	EndGameTimer = EndGameTimer - dt

	if EndGameTimer <= 0 then
		Player.p.dir = {x = 0, y = -1}
		Pickups.setCurrent(PickupTypes.normal)
		Satellites.discard()
		EndGameOpacity = 1
		bgSpeed.y = 0
		Twinklies.control.speed = 0
		PlanetSpeed = 0
	elseif EndGameTimer < 5 then
		Player.p.controlsDisabled = true
		Player.p.dir = {x = 0, y = 0}
		local frac = EndGameTimer/5
		bgSpeed.y = 100 * frac
		Twinklies.control.speed = 120 * frac
		stripesVisible = false
		PlanetSpeed = 110 * frac
	end

	PlanetPos.y = PlanetPos.y + PlanetSpeed * dt

	-- music volume
	local musfrac = math.min(math.max(0,(EndGameTimer-10)/5),1)
	Sound.ingameMusic:setVolume(0.7*musfrac)
	if EndGameTimer > 0 and EndGameTimer < 10 and not EndGameMusicPlaying then
		EndGameMusicPlaying = true
		playEndMusic()
	end
end

function drawEndGamePre()
	if not gameFinished then return end
	love.graphics.setColor(255,255,255,255*0.45)
	love.graphics.draw(PlanetBgImg,PlanetPos.x-fgOvr.x,PlanetPos.y)
	love.graphics.setColor(255,255,255)
	love.graphics.draw(PlanetFgImg,PlanetPos.x-fgOvr.x,PlanetPos.y)
end

function drawEndgame()
	if not gameFinished then return end
	if EndGameOpacity > 0 and not pauseScreen.paused then
		love.graphics.setColor(255,255,255,255*EndGameOpacity)
		printShadowedText("TEH END",screenSize.x / 2,screenSize.y / 4+40,EndGameOpacity,true)
		drawHighscores(EndGameOpacity)
	end
end
