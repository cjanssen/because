function initMushroom()
end

function startMushroom()
	-- mushrooms
	mushroom = {
		state = 0,
		scale = { x = 1, y = 1 },
		scaleAmp = 0.25,
		scalePhase = { x = math.random()*math.pi*2, y = math.random()*math.pi*2 },
		scaleFreq = { x = math.sqrt(2)*3, y = math.sqrt(3)*3 },
		currentHue = math.random()*360,
		oldHue = math.random()*360,
		newHue = math.random()*360,
		hueTimer = 0,
		hueDelay = 10,
		transitionTimer = 0,
		transitionDelay = 1.5,
		currentColor = { r = 255, g = 255, b = 255 }
	}
end

function drawMushroom()
	if mushroom.state ~= 0 then
		love.graphics.push()
		love.graphics.scale(mushroom.scale.x, mushroom.scale.y)
		love.graphics.setColor(mushroom.currentColor.r, mushroom.currentColor.g, mushroom.currentColor.b)
	end
end

function undrawMushroom()
	if mushroom.state ~= 0 then
		love.graphics.pop()
		love.graphics.setColor(255,255,255)
	end
end


function updateMushroom(dt)
	local activateMushroom = (Pickups.current == PickupTypes.mushroom)

	if mushroom.state == 0 then
		mushroom.currentColor = { r = 255, g = 255, b = 255 }
	end
	
	if activateMushroom and mushroom.state == 0 then
		mushroom.state = 1
		mushroom.transitionTimer = mushroom.transitionDelay
		enableMushroomSound()
	end

	if not activateMushroom and mushroom.state == 2 then
		mushroom.state = 3
		mushroom.transitionTimer = 0
		disableMushroomSound()
	end

	if not activateMushroom and mushroom.state == 1 then
		mushroom.state = 3
		disableMushroomSound()
	end

	-- start phase
	if mushroom.state == 1 then
		mushroom.transitionTimer = mushroom.transitionTimer - dt
		if mushroom.transitionTimer <= 0 then
			mushroom.state = 2
		end
	end

	-- end phase
	if mushroom.state == 3 then
		mushroom.transitionTimer = mushroom.transitionTimer + dt
		if mushroom.transitionTimer >= mushroom.transitionDelay then
			mushroom.state = 0
		end
	end

	if mushroom.state ~= 0 then
		-- activate totally
		local amp = mushroom.scaleAmp/2

		-- hue
		mushroom.hueTimer = mushroom.hueTimer - dt
		if mushroom.hueTimer <= 0 then
			mushroom.hueTimer = mushroom.hueDelay
			mushroom.oldHue = mushroom.newHue % 360
			mushroom.newHue = mushroom.oldHue + 60 + math.random()*300
		end
		local fraction = mushroom.hueTimer / mushroom.hueDelay
		mushroom.currentHue = (mushroom.oldHue * fraction + mushroom.newHue * (1-fraction)) % 360

		-- transitions
		if mushroom.state == 1 or mushroom.state == 3 then
			local fraction = mushroom.transitionTimer / mushroom.transitionDelay
			local rr,gg,bb,aa = hsv2rgb(mushroom.currentHue,255,255,255)
			mushroom.currentColor = {
				r = 255 * fraction + rr * (1-fraction),
				g = 255 * fraction + gg * (1-fraction),
				b = 255 * fraction + bb * (1-fraction)
			}
			amp = mushroom.scaleAmp * (1-fraction) / 2
		else
			local rr,gg,bb,aa = hsv2rgb(mushroom.currentHue,255,255,255)
			mushroom.currentColor = { r = rr, g = gg, b = bb }
		end

		-- scale
		mushroom.scalePhase.x = (mushroom.scalePhase.x + mushroom.scaleFreq.x * dt) % (math.pi*2)
		mushroom.scalePhase.y = (mushroom.scalePhase.y + mushroom.scaleFreq.y * dt) % (math.pi*2)

		mushroom.scale.x = 1 + ( 1 + math.sin(mushroom.scalePhase.x) ) * amp
		mushroom.scale.y = 1 + ( 1 + math.sin(mushroom.scalePhase.y) ) * amp
	end
end
