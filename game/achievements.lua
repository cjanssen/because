
-- 1. trigger happy: shoot 160 enemies in 60 seconds
-- 2. executioner: shoot 2000 enemies in 10 minutes
-- 3. xenocide: shoot 3500 enemies
-- 4. ferris wheel: use the rotation for 1 minute
-- 5. trippin: use the mushroom for 2 minutes
-- 6. gourmet: activate sushi+cheese+bottle in succession
-- 7. houseware: activate card+cat+flower in succession
-- 8. power hungry: activate X powerups (in the same game)
-- 9. gardener: destroy X enemies with the flower bomb
-- 10. dodge master: reach level N without dying once
-- 11. stealthking: reach level N without shooting any enemy
-- 12. pied piper: finish the game without shooting follower enemies (blue/yellow)
-- 13. bullfight: finish the game shooting only red enemies
-- 14. hardcore: finish the game X times in a row

-- time-based pickups: show x time / x total


function initAchievements()
	Cte = {
		triggerhappy = {
			enemies = 1000
		},
		executioner = {
			enemies = 3500
		},
		xenocide = {
			enemies = 10000
		},
		gourmet = {
			combo = 3
		},
		houseware = {
			combo = 3
		},
		ferriswheel = {
			delay = 120
		},
		trippin = {
			delay = 420
		},
		hungry = {
			count = 135
		},
		gardener = {
			enemies = 180
		},
		stealthking = {
			sector = 13
		},
		bullfight = {
			sector = 16
		},
		dodgemaster = {
			sector = 19
		},
		piedpiper = {
			sector = 28
		},
		hardcore = {
			times = 5
		}
	}

	Achievements = {
		strings = {
			{"TRIGGER HAPPY","SHOOT "..Cte.triggerhappy.enemies.." ENEMIES"},
			{"EXECUTIONER", "SHOOT "..Cte.executioner.enemies.." ENEMIES"},
			{"XENOCIDE", "SHOOT "..Cte.xenocide.enemies.." ENEMIES"},
			{"FERRIS WHEEL", "USE ROTATION FOR "..Cte.ferriswheel.delay.." SECONDS"},
			{"TRIPPIN", "USE MUSHROOM FOR "..math.ceil(Cte.trippin.delay/60).." MINUTES"},
			{"GOURMET", "ACTIVATE SUSHI+CHEESE+BOTTLE IN A ROW"},
			{"HOUSEWARE", "ACTIVATE CARD+CAT+FLOWER IN A ROW"},
			{"HUNGRY", "ACTIVATE "..Cte.hungry.count.." POWERUPS"},
			{"GARDENER", "DESTROY "..Cte.gardener.enemies.." ENEMIES WITH FLOWER BOMB"},
			{"STEALTH KING", "REACH SECTOR "..Cte.stealthking.sector.." WITHOUT SHOOTING ANY ENEMY"},
			{"BULLFIGHT", "REACH SECTOR "..Cte.bullfight.sector.." SHOOTING ONLY RED ENEMIES"},
			{"DODGE MASTER", "REACH SECTOR "..Cte.dodgemaster.sector.." WITHOUT DYING ONCE"},
			{"PIED PIPER", "REACH SECTOR "..Cte.piedpiper.sector.." WITHOUT SHOOTING FOLLOWER ENEMIES"},
			{"HARDCORE", "FINISH GAME "..Cte.hardcore.times.." TIMES"}
		},

		unlocked = {},
		data = {},
		spill = {},
		msgs = {},
		unlockCount = 0,
		total = 14
	}

	for i,v in ipairs(Achievements.strings) do
		Achievements.unlocked[i] = false
		Achievements.data[i] = false
		Achievements.spill[i] = true
	end

	Achievements.killCount = 0

	Achievements.spill[AchievTypes.dodgemaster] = false
	Achievements.spill[AchievTypes.stealthking] = false
	Achievements.spill[AchievTypes.piedpiper] = false
	Achievements.spill[AchievTypes.bullfight] = false
end

function startAchievements()
	Achievements.playing = false
	-- Achievements.killCount = 0
	Achievements.timer = 0

	Achievements.data[AchievTypes.gourmet] = false
	Achievements.data[AchievTypes.houseware] = false
	Achievements.data[AchievTypes.dodgemaster] =  true
	Achievements.data[AchievTypes.stealthking] =  true
	Achievements.data[AchievTypes.piedpiper] =  true
	Achievements.data[AchievTypes.bullfight] =  true

	if not Achievements.data[AchievTypes.hardcore] then
		Achievements.data[AchievTypes.hardcore] = 0
	end
end

function updateAchievements(dt)
	if gameState == 2 then
		Achievements.timer = Achievements.timer + dt

		if Pickups.current == PickupTypes.rotate then
			if type(Achievements.data[AchievTypes.ferriswheel]) ~= "number" then
				Achievements.data[AchievTypes.ferriswheel] = 0
			end
			Achievements.data[AchievTypes.ferriswheel] = Achievements.data[AchievTypes.ferriswheel] + dt
			if Achievements.data[AchievTypes.ferriswheel] > Cte.ferriswheel.delay then
				unlockAchievement(AchievTypes.ferriswheel)
			end
		end

		if Pickups.current == PickupTypes.mushroom then
			if type(Achievements.data[AchievTypes.trippin]) ~= "number" then
				Achievements.data[AchievTypes.trippin] = 0
			end
			Achievements.data[AchievTypes.trippin] = Achievements.data[AchievTypes.trippin] + dt
			if Achievements.data[AchievTypes.trippin] > Cte.trippin.delay then
				unlockAchievement(AchievTypes.trippin)
			end
		end
	end

	-- text fadein/out
	local anydead = false
	for i,msg in ipairs(Achievements.msgs) do
		msg.timer = msg.timer - dt
		if msg.step == 1 then
			msg.opacity = increaseExponential(dt, msg.opacity, 0.91)
			if msg.timer <= 0 then
				msg.timer = 3
				msg.step = 2
			end

		elseif msg.step == 2 then
			msg.opacity = 1
			if msg.timer <= 0 then
				msg.timer = 1
				msg.step = 3
			end
		elseif msg.step == 3 then
			msg.opacity = decreaseExponential(dt,msg.opacity,0.91)
			if msg.timer <= 0 then
				msg.opacity = 0
				msg.dead = true
				anydead = true
			end
		end
	end

	-- remove "dead" messages
	if anydead then
		local n = table.getn(Achievements.msgs)
		for i=1,n do 
			local k = n-i+1
			if Achievements.msgs[k].dead then
				table.remove(Achievements.msgs, k)
			end
		end
	end
end

function countAchievementUnlocks()
	local cnt = 0
	for i,unl in ipairs(Achievements.unlocked) do
		if unl then cnt = cnt + 1 end
	end
	Achievements.unlockCount = cnt
end

function unlockAchievement(ndx)
	if not Achievements.unlocked[ndx] then
		Achievements.unlockCount = Achievements.unlockCount + 1 
		table.insert(Achievements.msgs,{
			txt = "ACHIEVEMENT UNLOCKED : "..Achievements.strings[ndx][1],
			opacity = 0,
			step = 1,
			timer = 1
			})
		Achievements.unlocked[ndx] = true
		-- storeData()
	end
end

function registerEnemyKill(etype, btype)
	Achievements.killCount = Achievements.killCount + 1

	-- trigger happy
	Achievements.data[AchievTypes.triggerhappy] = Achievements.killCount
	if Achievements.killCount >= Cte.triggerhappy.enemies then
		unlockAchievement(AchievTypes.triggerhappy)
	end

	-- executioner
	Achievements.data[AchievTypes.executioner] = Achievements.killCount
	if Achievements.killCount >= Cte.executioner.enemies then
		unlockAchievement(AchievTypes.executioner)
	end

	-- xenocide
	if Achievements.killCount >= Cte.xenocide.enemies then
		unlockAchievement(AchievTypes.xenocide)
	end
	Achievements.data[AchievTypes.xenocide] = Achievements.killCount

	-- stealthking
	Achievements.data[AchievTypes.stealthking] = false

	-- pied piper
	if etype == EnemyTypes.spiral or etype == EnemyTypes.kamikaze then
		Achievements.data[AchievTypes.piedpiper] = false
	end

	-- bullfight
	if etype ~= EnemyTypes.normal and etype ~= EnemyTypes.snake and etype ~= EnemyTypes.mega then
		Achievements.data[AchievTypes.bullfight] = false
	end

	-- gardener
	if btype == BulletTypes.flower then
		if type(Achievements.data[AchievTypes.gardener]) ~= "number" then
			Achievements.data[AchievTypes.gardener] = 0
		end
		Achievements.data[AchievTypes.gardener] = Achievements.data[AchievTypes.gardener] + 1
		if Achievements.data[AchievTypes.gardener] >= Cte.gardener.enemies then
			unlockAchievement(AchievTypes.gardener)
		end
	end
end

function registerSectorReach(sectorNr)
	if sectorNr == Cte.dodgemaster.sector then
		if Achievements.data[AchievTypes.dodgemaster] then
			unlockAchievement(AchievTypes.dodgemaster)
		end
	end

	if sectorNr == Cte.stealthking.sector then
		if Achievements.data[AchievTypes.stealthking] then
			unlockAchievement(AchievTypes.stealthking)
		end
	end

	if sectorNr == Cte.piedpiper.sector then
		if Achievements.data[AchievTypes.piedpiper] then
			unlockAchievement(AchievTypes.piedpiper)
		end
	end

	if sectorNr == Cte.bullfight.sector then
		if Achievements.data[AchievTypes.bullfight] then
			unlockAchievement(AchievTypes.bullfight)
		end
	end
end

function registerStartGame()
	Achievements.playing = true
	Achievements.data[AchievTypes.triggerhappy] = Achievements.killCount
	Achievements.data[AchievTypes.executioner] = Achievements.killCount
	Achievements.data[AchievTypes.xenocide] = Achievements.killCount
end

function registerPlayerDied()
	Achievements.data[AchievTypes.dodgemaster] = false
end

function registerPickup(pickupid)
	if pickupid ~= PickupTypes.normal then
		if type(Achievements.data[AchievTypes.hungry]) ~= "number" then
			Achievements.data[AchievTypes.hungry] = 0
		end
		Achievements.data[AchievTypes.hungry] = Achievements.data[AchievTypes.hungry] + 1
		if Achievements.data[AchievTypes.hungry] >= Cte.hungry.count then
			unlockAchievement(AchievTypes.hungry)
		end

		-- gourmet
		if pickupid == PickupTypes.sushi then
			checkGourmetAchievement(1)
		elseif pickupid == PickupTypes.cheese then
			checkGourmetAchievement(2)
		elseif pickupid == PickupTypes.bottle then
			checkGourmetAchievement(3)
		-- elseif pickupid == PickupTypes.satellites then
		-- 	checkGourmetAchievement(4)
		else
			Achievements.data[AchievTypes.gourmet] = false
		end

		-- 7. houseware: activate card+cat+heart+flower in succession
		if pickupid == PickupTypes.card then
			checkHousewareAchievement(1)
		elseif pickupid == PickupTypes.cat then
			checkHousewareAchievement(2)
		elseif pickupid == PickupTypes.flower then
			checkHousewareAchievement(3)
		-- elseif pickupid == PickupTypes.heart then
		-- 	checkHousewareAchievement(4)
		else
			Achievements.data[AchievTypes.houseware] = false
		end	
	end
end

function checkGourmetAchievement(pickupNdx)
	if Achievements.data[AchievTypes.gourmet] == false then
		Achievements.data[AchievTypes.gourmet] = {false,false,false,count = 0}
	end

	if Achievements.data[AchievTypes.gourmet][pickupNdx] == false then
		Achievements.data[AchievTypes.gourmet][pickupNdx] = true
		Achievements.data[AchievTypes.gourmet].count = Achievements.data[AchievTypes.gourmet].count + 1
		if Achievements.data[AchievTypes.gourmet].count >= Cte.gourmet.combo then
			unlockAchievement(AchievTypes.gourmet)
		end
	end
end

function checkHousewareAchievement(pickupNdx)
	if Achievements.data[AchievTypes.houseware] == false then
		Achievements.data[AchievTypes.houseware] = {false,false,false,count = 0}
	end

	if Achievements.data[AchievTypes.houseware][pickupNdx] == false then
		Achievements.data[AchievTypes.houseware][pickupNdx] = true
		Achievements.data[AchievTypes.houseware].count = Achievements.data[AchievTypes.houseware].count + 1
		if Achievements.data[AchievTypes.houseware].count >= Cte.houseware.combo then
			unlockAchievement(AchievTypes.houseware)
		end
	end
end

function registerEndGame()
	Achievements.playing = false
	Achievements.data[AchievTypes.hardcore] = Achievements.data[AchievTypes.hardcore] + 1
	if Achievements.data[AchievTypes.hardcore] >= Cte.hardcore.times then
		unlockAchievement(AchievTypes.hardcore)
	end
end

function registerResetGame()
	Achievements.playing = false
end

function registerFailedGame()
	-- Achievements.data[AchievTypes.hardcore] = 0
end

function drawAchievements()
	local incy = font:getHeight()*1.6
	local iy = screenSize.y / 4 + incy

	for i,msg in ipairs(Achievements.msgs) do
		printShadowedText(msg.txt,
			math.floor(screenSize.x/2), 
			math.floor(iy+(i-1)*incy), msg.opacity, true )
	end
end

function printAchievementsTable(page)
	local y0 = screenSize.y/2-60
	local incy = 20

	printShadowedText("ACHIEVENTS UNLOCKED : "..Achievements.unlockCount.."/"..Achievements.total,
		screenSize.x/2, y0,1,true)

	local ndx0 = 0
	if page == 2 then ndx0 = 7 end
	for i=1,7 do
		local ndx = i + ndx0
		local op = 0.5
		if Achievements.unlocked[ndx] then 
			op = 1 
		end
		local yy = y0+incy*i

		printShadowedText(Achievements.strings[ndx][1], 200, yy, op, false)
		printShadowedText(Achievements.strings[ndx][2], 340, yy, op, false)

		-- special code
		if (Achievements.playing or Achievements.spill[ndx]) and not Achievements.unlocked[ndx] then
			if type(Achievements.data[ndx]) == "number" then
				-- numeric achievements
				if ndx == AchievTypes.triggerhappy then
					printShadowedText(Achievements.data[ndx].." / "..Cte.triggerhappy.enemies,
						100, yy, op, false)
				elseif ndx == AchievTypes.executioner then
					printShadowedText(Achievements.data[ndx].." / "..Cte.executioner.enemies,
						100, yy, op, false)
				elseif ndx == AchievTypes.xenocide then
					printShadowedText(Achievements.data[ndx].." / "..Cte.xenocide.enemies,
						100, yy, op, false)
				elseif ndx == AchievTypes.ferriswheel then
					printShadowedText(math.floor(Achievements.data[ndx]).." / "..math.floor(Cte.ferriswheel.delay).." S",
						100, yy, op, false)
				elseif ndx == AchievTypes.trippin then
					printShadowedText(math.floor(Achievements.data[ndx]).." / "..math.floor(Cte.trippin.delay).." S",
						100, yy, op, false)
				elseif ndx == AchievTypes.hungry then
					printShadowedText(Achievements.data[ndx].." / "..Cte.hungry.count,
						100, yy, op, false)
				elseif ndx == AchievTypes.gardener then
					printShadowedText(Achievements.data[ndx].." / "..Cte.gardener.enemies,
						100, yy, op, false)
				elseif ndx == AchievTypes.hardcore then
					printShadowedText(Achievements.data[ndx].." / "..Cte.hardcore.times,
						100, yy, op, false)
				end
			elseif type(Achievements.data[ndx]) == "boolean" then
				-- non-numeric achievements
				if ndx == AchievTypes.dodgemaster then
					if Achievements.data[ndx] then
						printShadowedText("IN PROGRESS", 100, yy, op, false)
					else
						printShadowedText("FAILED", 100, yy, op, false)
					end
				elseif ndx == AchievTypes.stealthking then
					if Achievements.data[ndx] then
						printShadowedText("IN PROGRESS", 100, yy, op, false)
					else
						printShadowedText("FAILED", 100, yy, op, false)
					end
				elseif ndx == AchievTypes.piedpiper then
					if Achievements.data[ndx] then
						printShadowedText("IN PROGRESS", 100, yy, op, false)
					else
						printShadowedText("FAILED", 100, yy, op, false)
					end
				elseif ndx == AchievTypes.bullfight then
					if Achievements.data[ndx] then
						printShadowedText("IN PROGRESS", 100, yy, op, false)
					else
						printShadowedText("FAILED", 100, yy, op, false)
					end
				end
			end
			-- special: gourmet / houseware
			if ndx == AchievTypes.gourmet then
				local c = 0
				if type(Achievements.data[ndx]) == "table" then
					c = Achievements.data[ndx].count
				end
				printShadowedText(c.." / "..Cte.gourmet.combo, 100, yy, op, false)
			elseif ndx == AchievTypes.houseware then
				local c = 0
				if type(Achievements.data[ndx]) == "table" then
					c = Achievements.data[ndx].count
				end
				printShadowedText(c.." / "..Cte.houseware.combo, 100, yy, op, false)
			end
		end
	end
end

function achievementPageCount()
	return math.ceil(Achievements.total / 7)
end

function serializeAchievements()
	local str = ""
	local total = 0
	for i,unl in ipairs(Achievements.unlocked) do
		if unl then
			str = str..string.gsub(Achievements.strings[i][1]," ","_").." = true,\n"
			total = total + 1
		end
	end
	local dtastr = ""
	local dtatotal = 0
	for i,dta in ipairs(Achievements.data) do
		local nam = string.gsub(Achievements.strings[i][1]," ","_")
		if type(dta) == "boolean" then
			local bst = "true"
			if not dta then bst = "false" end
			dtastr = dtastr..nam.." = "..bst..",\n"
			dtatotal = dtatotal + 1
		elseif type(dta) == "number" then
			dtastr = dtastr..nam.." = "..dta..",\n"
			dtatotal = dtatotal + 1
		end
	end
	str = str .. "DATA = {\n"..dtastr.."TOTAL = "..dtatotal.."\n},\n"
	str = str.."TOTAL = "..total.."\n"
	str = "AchievementList = {\n"..str.."}\n"
	return str
end

function parseAchievements()
	if not AchievementList then return end
	for i,unl in ipairs(Achievements.unlocked) do
		local aname = string.gsub(Achievements.strings[i][1]," ","_")
		if AchievementList[aname] then Achievements.unlocked[i] = true end
	end

	local dtakey = "DATA"
	if AchievementList[dtakey] and type(AchievementList[dtakey]) == "table" then
		local tab = AchievementList[dtakey]
		for i,dta in ipairs(Achievements.data) do
			local nam = string.gsub(Achievements.strings[i][1]," ","_")
			if tab[nam] ~= nil and type(tab[nam]) == "number" or type(tab[nam]) == "boolean" then
				Achievements.data[i] = tab[nam]
			end
		end
	end

	if type(Achievements.data[AchievTypes.xenocide]) == "number" then
		Achievements.killCount = Achievements.data[AchievTypes.xenocide]
	end

	countAchievementUnlocks()

	AchievementList = nil
end

function wipeAchievements()
	local p = Achievements.playing
	local t = Achievements.timer
	AchievementList = nil
	initAchievements()
	startAchievements()
	Achievements.playing = p
	Achievements.timer = t

	if p then
		Achievements.data[AchievTypes.triggerhappy] = 0
		Achievements.data[AchievTypes.executioner] = 0
		Achievements.data[AchievTypes.xenocide] = 0
		Achievements.data[AchievTypes.ferriswheel] = 0
		Achievements.data[AchievTypes.trippin] = 0
		Achievements.data[AchievTypes.hungry] = 0
		Achievements.data[AchievTypes.gardener] = 0
		Achievements.data[AchievTypes.hardcore] = 0

	end
end

function boolToString(b)
	if b then return "TRUE" else return "FALSE" end
end

function drawAchievementsTmp()
	-- love.graphics.setColor(255,255,255)
	-- love.graphics.print(math.floor(Achievements.killCount),10,100)
	-- love.graphics.print(math.floor(Achievements.timer),10,120)

	-- if Achievements.data[AchievTypes.gourmet] ~= false then
	-- 	love.graphics.print("GO "..Achievements.data[AchievTypes.gourmet].count,10,140)
	-- else
	-- 	love.graphics.print("GO 0",10,140)
	-- end
	-- if Achievements.data[AchievTypes.houseware] ~= false then
	-- 	love.graphics.print("HW "..Achievements.data[AchievTypes.houseware].count,10,160)
	-- else
	-- 	love.graphics.print("HW 0",10,160)
	-- end



	-- -- love.graphics.print("BF "..boolToString(Achievements.data[AchievTypes.bullfight]),10,130)
	-- if type(Achievements.data[AchievTypes.hungry])=="boolean" then
	-- 	love.graphics.print(boolToString(Achievements.data[AchievTypes.hungry]),10,180)
	-- else
	-- 	love.graphics.print("PU "..Achievements.data[AchievTypes.hungry],10,180)
	-- end

	-- if type(Achievements.data[AchievTypes.ferriswheel])=="boolean" then
	-- 	love.graphics.print(boolToString(Achievements.data[AchievTypes.ferriswheel]),10,200)
	-- else
	-- 	love.graphics.print("FW "..math.floor(Achievements.data[AchievTypes.ferriswheel]),10,200)
	-- end

	-- if type(Achievements.data[AchievTypes.trippin])=="boolean" then
	-- 	love.graphics.print(boolToString(Achievements.data[AchievTypes.trippin]),10,220)
	-- else
	-- 	love.graphics.print("MU "..math.floor(Achievements.data[AchievTypes.trippin]),10,220)
	-- end

	-- if type(Achievements.data[AchievTypes.gardener])=="boolean" then
	-- 	love.graphics.print(boolToString(Achievements.data[AchievTypes.gardener]),10,240)
	-- else
	-- 	love.graphics.print("GA "..Achievements.data[AchievTypes.gardener],10,240)
	-- end
	
	-- love.graphics.print(boolToString(Achievements.unlocked[1]),10,120)
	-- if type(Achievements.data[AchievTypes.hungry]) == "number" then
	-- 	love.graphics.print(Achievements.data[AchievTypes.hungry],10,100)
	-- end
	-- love.graphics.print(math.floor(Achievements.timer),10,120)
	-- love.graphics.print(Achievements.killZero,10,140)
end
