function initDifficulty()
	if not difficulty then
		difficulty = {}
		difficulty.value = 1
		difficulty.currentEnemy = 1
		difficulty.enemyNumber = 12
		difficultyLoad()
	end
end

function drawDifficulty()
	love.graphics.setColor(255,255,255)
	love.graphics.print(difficulty.value, 20, 50)
end

function updateDifficulty(dt)
end

function getRandVal(lowest,highest,var,compression,isint,limits)
	-- lowest is value at difficulty = 1
	-- highest is value at difficulty = 10
	compression = compression or 1
	local v = math.pow((difficulty.value-1)/9,compression) * (highest-lowest) + lowest
	local d = 1 + (math.random()-0.5) * var
	local r = v*d
	if limits then
		r = math.min(math.max(r,limits[1]),limits[2])
	end
	if isint then
		return math.floor(r)
	else
		return r
	end
end

-- returns true with a given probability
function getRandBool(probLow,probHigh,var,compression)
	local prob = getRandVal(probLow,probHigh,var,compression)
	if math.random() < prob then
		return not reversed
	else
		return reversed
	end
end

function getRandFunc(lowest,highest,var,compression,isint,limits)
	return function() return getRandVal(lowest,highest,var,compression,isint,limits) end
end

function getRandBoolFunc(probLow,probHigh,var,compression)
	return function() return getRandBool(probLow,probHigh,var,compression) end
end

function getConstantFunc(v)
	return function() return v end
end

function difficultyLoad()
	if not difficulty then initDifficulty() end
	params = {}
	-- todo: reduce this at the beginning
	params.newSpawnerDelay = getRandFunc(2,1.4,0.3,0.5)
	params.newPickupSpawnCount = getRandFunc(6,36,1,0.6,true,{6,40})
	params.waveTime = getRandFunc(35,15,0.2,0.85)
	params.waveBreak = getRandFunc(4,8,0.3,0.85)

	params.rowCount = getRandFunc(1,5,0.15,0.5,true,{1,10})
	params.colCount = getRandFunc(1,8,0.3,0.5,true,{1,10})
	params.lineCount = getRandFunc(1,12,0.2,0.7)
	params.individualCount = getRandFunc(2.5,5.5,0.2,0.7)
	params.enemyMultipliers = { 4, 3, 0.75, 1, 1.5, 1, 1, 1.5,  0.5,  3, 1,  2 }


	params.normalEnemyShootDelay = getRandFunc(2.5,0.5,0.1,0.8)

	params.snakeEnemyShootDelay = getRandFunc(1.7,0.25,0.3,0.8)
	params.snakeEnemyOscillationSpeed = getRandFunc(4,5,0.4,0.4)
	params.snakeEnemyOscillationAmplitude = getRandFunc(40,85,0.4,0.8)

	params.megaEnemyShootDelay = getRandFunc(1.3,0.4,0.1,0.4)
	params.megaEnemyShootLoop = getRandFunc(6,4,0.05,0.7,true,{3,6})
	params.megaEnemyLaserAmount = getRandFunc(4,9,0.05,0.7,true,{4,10})
	params.megaEnemyLaserEnabled = getConstantFunc(true)
	params.megaEnemyHomingEnabled = getRandBoolFunc(0.5,1,0.5,0.5)

	params.millEnemyRotationCount = getRandFunc(6,3,0.05,0.4,true,{3,6})
	params.millEnemyShootDelay = getRandFunc(1.3,0.8,0.1,0.6)
	params.millEnemyFirstDelay = getRandFunc(1.3,0.8,0.1,0.6)
	params.millEnemySecondDelay = getRandFunc(0.2,0.04,0.1,0.4)
	params.millEnemyRotTime = getRandFunc(2,1,0.15,0.75)
	params.millEnemyLaserEnabled = getConstantFunc(true)

	params.bishopEnemyShootDelay = getRandFunc(1.5,0.6,0.25,0.75)
	params.bishopEnemyBulletCount = getRandFunc(1,7,0.2,0.75,true,{1,8})
	params.bishopEnemyBulletSeparation = getRandFunc(10,6,0.05,0.4)

	params.kamikazeEnemyMaxSteer = getRandFunc(1*math.pi/180,5*math.pi/180,0.2,1.1)
	params.kamikazeEnemyExplosionDelay = getRandFunc(1,3,0.4,1.3)
	params.kamikazeEnemySpeedup = getRandFunc(1,1.8,0.1,1.3)
	params.chickenGroupSize = getRandFunc(3,7,0.3,0.4,true,{3,8})

	params.hawkEnemyShootDelay = getRandFunc(0.8,0.24,0.4,0.7)
	params.raspberryBulletSpeed = getConstantFunc(50 + math.random()*100)
	params.raspberryBulletLife = getRandFunc(1.5,5,1.1,1,false,{1.5,5})

	params.chickenEnemySlowdown = getConstantFunc(0.9)
	params.chickenEnemyThreshold = getConstantFunc(10)

	params.spiralEnemyShootDelay = getRandFunc(1.5,0.5,0.05,1)
	params.spiralEnemyAttraction = getRandFunc(0.2,0.6,0.3,1.2)
	params.spiralEnemySpeed = getRandFunc(220,300,0.1,1.3)

	params.plantEnemyShootDelay = getRandFunc(3,1.8,0.4,0.7)
	params.plantBulletSpacing = getRandFunc(1.8,0.7,0.2,1)
	params.plantBulletCount = getRandFunc(4,10,0.15,0.6)


	params.clockEnemyShootDelay = getRandFunc(1.8,0.9,0.1,0.8)
	params.clockEnemyInitialAdv = getRandFunc(1,1,3,1,true,{0,3})
	params.clockEnemySeqLen = getRandFunc(2,6,1,0.9,true,{1,8})
	params.clockEnemyRowCount = getRandFunc(2,7,1,0.9,true,{1,9})
	params.clockEnemySep = getRandFunc(40,40,2,1,true,{24,60})

	params.falconEnemyAdvanceTime = getRandFunc(1.8,1.3,0.1,1)
	params.falconEnemyDownDelay = getRandFunc(1,0.9,0.3,0.6)
	params.falconEnemyRotDelay = getRandFunc(1.2,0.9,0.05,0.88)
	params.falconEnemyShootingDelay = getRandFunc(0.9,1.5,0.2,1.4)
	params.falconEnemySeparation = getConstantFunc(100)
	params.falconEnemyKnockbackRate = getConstantFunc(0.6)
	params.falconEnemyKnockbackSpeed = getConstantFunc(300)
	params.falconEnemyShootDelay = getRandFunc(0.9,0.45,0.3,0.7)
	params.falconEnemyMgDelay = getRandFunc(0.4,0.1,0.3,0.5)
	params.falconEnemyNormalDelay = getRandFunc(0.9,0.45,0.3,0.7)
	params.mgBulletSpeed = getRandFunc(400,600,0.3,0.5)
	params.mgBulletXBias = getRandFunc(12,48,0.3,0.5)
	params.mgBulletYBias = getRandFunc(4,16,0.3,0.5)

	params.swarmEnemyShootDelay = getRandFunc(2.2,0.7,0.2,0.55)
	params.swarmEnemyBaseSpeed = getConstantFunc(100)
	params.swarmEnemyVariableSpeed = getRandFunc(120,160,0.1,0.8)
	params.swarmEnemySwitchFactor = getConstantFunc(0.96)
	params.swarmEnemySwitchTimer = getRandFunc(3.5,1.6,0.5,0.75)
	params.swarmEnemySwitchDelay = getRandFunc(2.25,1.60,0.1,0.9)
	params.swarmEnemySwitchDelayDown = getRandFunc(2.25,1.60,0.1,0.9)
	params.swarmEnemySwitchDelayUp = getRandFunc(2.25,1.60,0.1,0.9)


	params.fasterBulletSpeed = getConstantFunc(400)
	params.homingBulletSpeed = getConstantFunc(300)
	params.laserBulletSpeed = getConstantFunc(300)



end
