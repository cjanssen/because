Explo = {}

function startExplo()
	Explo.list = {}
end

function initExplo()
	Explo.initFrames()
end

function Explo.initFrames()
	local img = love.graphics.newImage("img/explosion2.png")
	Explo.frames = {}
	Explo.batch = love.graphics.newSpriteBatch(img,400,"static")

	for k=0,8 do
		local i = k%4
		local j = 3-math.floor(k/4)
		table.insert(Explo.frames,
			love.graphics.newQuad(i*128, j*128, 128, 128, img:getWidth(), img:getHeight()))
	end
end

function Explo.add(startPos, smallExplo)
	if smallExplo then
		kickVibration(0.075)
	else
		kickVibration(0.15)
	end

	local delay = 0.1
	table.insert(Explo.list, {
		pos = { x = startPos.x, y = startPos.y },
		frameNdx = 1,
		frameTimer = delay,
		frameDelay = delay,
		speed = 100,
		valid = true
		})
end

function Explo.update(dt)
	for i,v in ipairs(Explo.list) do
		-- pos
		v.pos.y = v.pos.y + v.speed * dt
		-- frames
		v.frameTimer = v.frameTimer - dt
		if v.frameTimer <= 0 then
			v.frameTimer = v.frameDelay
			v.frameNdx = v.frameNdx + 1
			if v.frameNdx > table.getn(Explo.frames) then
				v.valid = false
			end
		end
	end

	-- purge
	local n = table.getn(Explo.list)
	for i=1,n do
		local v = Explo.list[n-i+1]
		if not v.valid then
			table.remove(Explo.list,n-i+1)
		end
	end
end

function Explo.draw()
	Explo.batch:clear()
	for i,v in ipairs(Explo.list) do
		-- Explo.batch:add(Explo.frames[v.frameNdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), 0, 1, 1, 64, 64)
		addToBatch(Explo.batch,Explo.frames[v.frameNdx], math.floor(v.pos.x-fgOvr.x), math.floor(v.pos.y), 0, 1, 1, 64, 64)
	end
	love.graphics.draw(Explo.batch)
end